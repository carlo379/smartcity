//
//  CommentsViewController.m
//  SmartCity
//
//  Created by Carlos Martinez on 3/28/16.
//  Copyright © 2016 Carlos Martinez. All rights reserved.
//

#import "CommentsViewController.h"
#import "HCSStarRatingView.h"
#import "Firebase.h"
#import "LoginViewController.h"
#import "CommentsTableViewCell.h"

#pragma mark - Interface
@interface CommentsViewController ()<UITextViewDelegate, UITableViewDelegate, UITableViewDataSource>

#pragma mark Properties
@property (weak, nonatomic) IBOutlet UILabel *textViewLabel;
@property (weak, nonatomic) IBOutlet UITextView *commentsTextView;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *starRatingView;
@property (strong, nonatomic) Firebase *dbRef;
@property (strong, nonatomic) Firebase *commentRef;
@property (strong, nonatomic) NSMutableArray *commentArray;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSString *loggedUser;

@end

#pragma mark Implementation
@implementation CommentsViewController

#pragma mark - View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Login user before commenting
    [self login];
    
    // Listen for changes to data in Firebase Backend
    [self listenToFirebaseDb];
    
    // Init an Array to store Firebase Data
    self.commentArray = [[NSMutableArray alloc]init];
    
    // Gesture recognizer to hide keyboard when user touch tableView
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.tableView addGestureRecognizer:gestureRecognizer];
    
    // Base URL for Firebase
    self.dbRef = [[Firebase alloc] initWithUrl:@"https://smarttree.firebaseio.com"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBActions
- (IBAction)submitPressed:(UIButton *)sender {
    
    // Hide Keyboard
    [self.commentsTextView resignFirstResponder];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.loggedUser = [defaults valueForKey:@"LoggedUser"];
    
    // If not logged, return and present Login View COntroller
    if(!self.loggedUser){
        [self login];
        return;
    }
    
    // Prepare Data to add to Dictionay before uploading to Firebase Backend
    NSString *tempText = self.commentsTextView.text;
    NSNumber *tempRating = @(self.starRatingView.value);
    
    // Create a reference to a Firebase database URL
    self.commentRef = [self.dbRef childByAppendingPath: @"comments"];
    Firebase *commentRefAutoID = [self.commentRef childByAutoId];

    // Write data to Firebase in Dictionary
    NSDictionary *commentsDict = @{
                                   @"Comment" : tempText,
                                   @"Rating" : tempRating,
                                   @"User" : self.loggedUser,
                                };
    
    // Post to Firebase Database
    [commentRefAutoID setValue: commentsDict];
    
    // Erase TextView Text & Hide Instructions Label
    self.commentsTextView.text = @"";
    self.textViewLabel.hidden = NO;
    
}

- (IBAction)cancelPressed:(UIBarButtonItem *)sender {
    // Dismiss Comments View COntroller
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];

}

#pragma mark - Custom Methods
- (void) hideKeyboard {
    
    // Hide Keyboard, Clear TextView and Show Instructions Label
    [self.commentsTextView resignFirstResponder];
    self.commentsTextView.text = @"";
    self.textViewLabel.hidden = NO;
}

- (void)listenToFirebaseDb {

    // Get Reference to the Firebase Database
    Firebase *ref = [[Firebase alloc] initWithUrl: @"https://smarttree.firebaseio.com/comments"];
    
    // Attach a block to read the data at our posts reference
    [ref observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
        
        if(snapshot.value == [NSNull null]) {
            NSLog(@"No messages");
        } else {
            // Convert Firebase objects to an Array of Objects for use in TableView
            [self.commentArray removeAllObjects];
            for (FDataSnapshot* childSnap in snapshot.children) {
                // Add each object to array
                [self.commentArray addObject:childSnap];
            }
        }
        
        // Reload Table after Fetching objects from Backend
        [self.tableView reloadData];
        
    } withCancelBlock:^(NSError *error) {
        NSLog(@"%@", error.description);
    }];
}

#pragma mark - TextView Delegate Methods
- (void)textViewDidBeginEditing:(UITextView *)textView {
    
    // Hide Instructions Label When user begin typing
    self.textViewLabel.hidden = YES;
}

#pragma mark - Login Sequence Methods
- (void)login {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.loggedUser = [defaults objectForKey:@"LoggedUser"];
    
    // If not user logged, present Login View Controller
    if (!self.loggedUser) {
        NSString * storyboardName = @"Main";
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
        UIViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
        [self presentViewController:vc animated:YES completion:nil];
    }
}

- (void)logout {
    // Clear Logged user data
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:nil forKey:@"LoggedUser"];
    self.loggedUser = nil;
}

#pragma mark - TableViewDataSource Protocol Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return quantity of sections fetched
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Qty of elements on array from Firebase
    return [self.commentArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // we use a nib which contains the cell's view and this class as the files owner
    [tableView registerNib:[UINib nibWithNibName:@"TableCell" bundle:nil] forCellReuseIdentifier:@"commentsCellIdentifier"];
    
    // Get a referene Cell
    CommentsTableViewCell *cell = (CommentsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"commentsCellIdentifier" forIndexPath:indexPath];
    
    // Get Each Object an construct Cell for presenting on TableView
    FDataSnapshot *snapObject = [self.commentArray objectAtIndex:indexPath.row];
    cell.commentLabel.text = snapObject.value[@"Comment"];
    cell.ratingView.value = [snapObject.value[@"Rating"] doubleValue];
    cell.ratingView.userInteractionEnabled = NO;
    
    return cell;
}

@end
