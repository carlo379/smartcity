//
//  LoginViewController.m
//  SmartCity
//
//  Created by Carlos Martinez on 3/28/16.
//  Copyright © 2016 Carlos Martinez. All rights reserved.
//

#import "LoginViewController.h"
#import "Firebase.h"
#import "Constants.h"

#pragma mark - interface
@interface LoginViewController ()<UITextFieldDelegate>

#pragma mark Properties
@property (weak, nonatomic) IBOutlet UITextField *emailTextfield;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIImageView *emailErrorImageView;
@property (weak, nonatomic) IBOutlet UIImageView *passwordErrorImageView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loginSpinner;


@end

#pragma mark - Implementation
@implementation LoginViewController

#pragma mark - View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *user = [defaults objectForKey:@"LoggedUser"];
    
    if(user)
        [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - IBAction
- (IBAction)loginPressed:(UIButton *)sender {
    // Start Spinner to signal user of Start of Login sequence
    [self.loginSpinner startAnimating];
    
    Firebase *ref = [[Firebase alloc] initWithUrl:@"https://smarttree.firebaseio.com"];
    [ref authUser:self.emailTextfield.text password:self.passwordTextField.text withCompletionBlock:^(NSError *error, FAuthData *authData) {
    if (error) {
        // There was an error logging in to this account
        self.emailErrorImageView.hidden = NO;
        self.passwordErrorImageView.hidden = NO;
    } else {
        // We are now logged in
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setValue:authData.uid forKey:@"LoggedUser"];
        
        if([authData.uid isEqualToString:kAdministratorUID]){
            
            NSString * storyboardName = @"Main";
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
            UIViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"AdministratorViewController"];
            [self presentViewController:vc animated:YES completion:nil];
        } else {
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
        // Stop Spinner Animation
        [self.loginSpinner stopAnimating];

}];
}

- (IBAction)cancelPressed:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - UITextFieldDelegate Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if(textField == self.emailTextfield) {
        self.emailErrorImageView.hidden = YES;
    } else {
        self.passwordErrorImageView.hidden = YES;
    }
}

@end
