//
//  AdministratorViewController.m
//  SmartCity
//
//  Created by Carlos Martinez on 4/28/16.
//  Copyright © 2016 Carlos Martinez. All rights reserved.
//

#import "AdministratorViewController.h"
@import AVFoundation;
#import "Constants.h"
#import "Firebase.h"
#import <CoreLocation/CoreLocation.h>
#import "HelpMethods.h"
#import "BEMSimpleLineGraphView.h"

enum MaintenanceActions{
    SensorRemoval,
    SensorReplace,
    TreeRemoval,
    TreeReplace
};

#pragma mark - Constants
@interface AdministratorViewController ()<AVCaptureMetadataOutputObjectsDelegate, CLLocationManagerDelegate, BEMSimpleLineGraphDataSource, BEMSimpleLineGraphDelegate>

@property (strong, nonatomic) AVAudioPlayer *audioPlayer;
@property (strong, nonatomic) AVCaptureSession *captureSession;
@property (strong, nonatomic) AVCaptureVideoPreviewLayer *videoPreviewLayer;

@property (weak, nonatomic) IBOutlet UIButton *treeButton;
@property (weak, nonatomic) IBOutlet UILabel *treeLabel;

@property (weak, nonatomic) IBOutlet UIButton *bulbButton;
@property (weak, nonatomic) IBOutlet UILabel *bulbLabel;

@property (weak, nonatomic) IBOutlet UIButton *speakerButton;
@property (weak, nonatomic) IBOutlet UILabel *speakerLabel;

@property (weak, nonatomic) IBOutlet UIButton *addTreeButton;
@property (weak, nonatomic) IBOutlet UIButton *treeMaintButton;
@property (weak, nonatomic) IBOutlet UIButton *treeDataButton;

@property (weak, nonatomic) IBOutlet UIButton *sensorAddButton;
@property (weak, nonatomic) IBOutlet UIButton *sensorMaintButton;
@property (weak, nonatomic) IBOutlet UIButton *sensorDataButton;

@property (weak, nonatomic) IBOutlet UIView *dashboardView;
@property (weak, nonatomic) IBOutlet UIView *qrPreviewView;
@property (weak, nonatomic) IBOutlet UIView *navBar;

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) CLLocation *currentLocation;
@property (nonatomic) CLLocationCoordinate2D userCoordinate;

@property (strong, nonatomic) NSMutableDictionary *treeURLs;

// Raw Data
@property (strong, nonatomic) NSMutableArray *treeArray;
@property (strong, nonatomic) NSMutableArray *treeSensorsArray;

// Store Data Individually
@property (strong, nonatomic) NSMutableArray *treeDataArray;
@property (strong, nonatomic) NSMutableArray *speakerDataArray;
@property (strong, nonatomic) NSMutableArray *bulbDataArray;

@property (strong, nonatomic) NSMutableDictionary *treeDataDict;
@property (strong, nonatomic) NSMutableArray *treeSensorDataArray;

@property (strong, nonatomic) HelpMethods *help;

@property (nonatomic) BOOL maintenanceFlag;
@property (nonatomic) enum MaintenanceActions maintenaceAction;

@property (strong, nonatomic) Firebase *mainRef;

@property (weak, nonatomic) IBOutlet BEMSimpleLineGraphView *chartView;
@property (strong, nonatomic) NSMutableArray *graphDataArray;
@property (strong, nonatomic) NSMutableArray *arrayOfDates;

@end

@implementation AdministratorViewController

#pragma mark - View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Signal controller that need to update Status Bar for change in color
    [self setNeedsStatusBarAppearanceUpdate];
    
    // Request autorization to get user location
    if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [self.locationManager requestAlwaysAuthorization];
    }
    
    self.treeURLs = [[NSMutableDictionary alloc]init];
    
    // Tree Data
    self.treeArray = [[NSMutableArray alloc]init];
    self.treeSensorsArray = [[NSMutableArray alloc]init];
    self.treeDataDict = [[NSMutableDictionary alloc]init];
    self.treeSensorDataArray = [[NSMutableArray alloc]init];
    self.treeDataArray = [[NSMutableArray alloc]init];
    self.bulbDataArray = [[NSMutableArray alloc]init];
    self.speakerDataArray = [[NSMutableArray alloc]init];
    
    //Initialize Location Manager
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    self.locationManager.delegate = self;
    
    // Listen for changes to data in Firebase Backend
    [self listenToFirebaseDbForTreeEvents];
    
    // Instantiate Helper object
    self.help = [[HelpMethods alloc]init];
    
    // Initialize Graph Data
    //self.graphDataArray = [@[@5.1, @6.1, @7.4, @5.2, @8.2, @9.2, @8.2, @7.2] mutableCopy];

    // SetUp Graph
    [self setupGraph];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    // Stop Updating Location
    [self.locationManager stopUpdatingLocation];
}

#pragma mark - UIViewController Methods Override
- (UIStatusBarStyle)preferredStatusBarStyle {
    // Update color/style of StatusBar
    return UIStatusBarStyleLightContent;
}

- (void)setupGraph {
    // Create a gradient to apply to the bottom portion of the graph
    CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
    size_t num_locations = 2;
    CGFloat locations[2] = { 0.0, 1.0 };
    CGFloat components[8] = {
        1.0, 1.0, 1.0, 1.0,
        1.0, 1.0, 1.0, 0.0
    };
    
    // Apply the gradient to the bottom portion of the graph
    self.chartView.gradientBottom = CGGradientCreateWithColorComponents(colorspace, components, locations, num_locations);
    
    // Enable and disable various graph properties and axis displays
    self.chartView.enableTouchReport = YES;
    self.chartView.enablePopUpReport = YES;
    self.chartView.enableYAxisLabel = YES;
    self.chartView.autoScaleYAxis = YES;
    self.chartView.alwaysDisplayDots = NO;
    self.chartView.enableReferenceXAxisLines = YES;
    self.chartView.enableReferenceYAxisLines = YES;
    self.chartView.enableReferenceAxisFrame = YES;
    
    // Draw an average line
    self.chartView.averageLine.enableAverageLine = YES;
    self.chartView.averageLine.alpha = 0.6;
    self.chartView.averageLine.color = [UIColor darkGrayColor];
    self.chartView.averageLine.width = 2.5;
    self.chartView.averageLine.dashPattern = @[@(2),@(2)];
    
    // Set the graph's animation style to draw, fade, or none
    self.chartView.animationGraphStyle = BEMLineAnimationDraw;
    
    // Dash the y reference lines
    self.chartView.lineDashPatternForReferenceYAxisLines = @[@(2),@(2)];
    
    // Show the y axis values with this format string
    self.chartView.formatStringForValues = @"%.1f";
}
#pragma mark - IBActions
- (IBAction)addTreePressed:(UIButton *)sender {
    
    // Start Reading QRCodes
    [self startReadingQRCodes];
}

- (IBAction)logoutPressed:(UIButton *)sender {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:nil forKey:@"LoggedUser"];
    
    // Dismiss Dashboard
    [self dismissViewControllerAnimated:YES completion:^{
        [self dismissViewControllerAnimated:NO completion:nil];
    }];
}


#pragma mark - Custom Methods
- (void)listenToFirebaseDbForTreeEvents {
    
    // Get Reference to the Firebase Database
    self.mainRef = [[Firebase alloc] initWithUrl: @"https://smarttree.firebaseio.com/tree"];
    
    // Attach a block to read the data at our posts reference
    [self.mainRef observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
        
        if(snapshot.value == [NSNull null]) {
            NSLog(@"No messages");
        } else {
            for (FDataSnapshot* childSnap in snapshot.children) {
                [self.treeArray addObject:childSnap.key];
                self.treeDataDict = childSnap.value;
                
                
                NSDictionary *dict = childSnap.value;
                NSLog(@"Dict %@", dict);
                
                NSLog(@"Dict elements %@", dict.allKeys);
                
                if([dict.allKeys containsObject:@"sensor"]){
                    NSDictionary *sensorsDict = dict[@"sensor"];
                    NSLog(@"Dict elements %@", sensorsDict.allKeys);
                    self.treeSensorsArray = [sensorsDict.allKeys mutableCopy];
                    for(NSDictionary *sensor in sensorsDict){
                        [self.treeSensorDataArray addObject:sensor];
                    }
                }
                
                
            }
            // Prepare Dashboard
            [self prepareDashboard];
        }
        NSLog(@"FInish Parsing");
    } withCancelBlock:^(NSError *error) {
        NSLog(@"%@", error.description);
    }];
}

- (void)prepareDashboard {
    for(NSString *tree in self.treeArray){
        self.treeButton.hidden = NO;
        self.treeLabel.text = tree;
        self.treeMaintButton.enabled = YES;
        self.sensorAddButton.enabled = YES;
        self.sensorDataButton.enabled = YES;
        self.treeDataButton.enabled = YES;
        NSString *pathURL = [@"tree/" stringByAppendingString:tree];
        self.treeURLs[tree] = pathURL;
    }
    
    for(int i=0;i<self.treeSensorsArray.count;i++){
        NSDictionary *sensorsDict = self.treeDataDict[@"sensor"];
        NSArray *sensorKeyArray = sensorsDict.allKeys;
        for(NSString *key in sensorKeyArray) {
            NSDictionary *maintenance = sensorsDict[key];
            NSDictionary *maintData = maintenance[@"maintenance"];
            NSDictionary *autoID = maintData[[maintData.allKeys objectAtIndex:0]];

            NSString *type = autoID[@"type"];
            if([type isEqualToString:@"speaker"]){
                self.speakerButton.hidden = NO;
                self.speakerLabel.text = key;
                self.sensorAddButton.enabled = YES;
                self.sensorMaintButton.enabled = YES;
            }
            if([type isEqualToString:@"bulb"]){
                self.bulbButton.hidden = NO;
                self.bulbLabel.text = key;
                self.sensorAddButton.enabled = YES;
                self.sensorMaintButton.enabled = YES;
            }
        }
        
        
    }
}

- (BOOL)startReadingQRCodes {
    
    // Initialize Capture Device (Video) and check for errors
    NSError *error;
    AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:&error];
    if (!input) {
        NSLog(@"%@", [error localizedDescription]);
        return NO;
    }
    
    // Initialize Capture Session and pass the Input Device (Video)
    self.captureSession = [[AVCaptureSession alloc] init];
    [self.captureSession addInput:input];
    AVCaptureMetadataOutput *captureMetadataOutput = [[AVCaptureMetadataOutput alloc] init];
    [self.captureSession addOutput:captureMetadataOutput];
    
    // Create a Queue to capture output on a different Thread and Specify QR Codes
    dispatch_queue_t dispatchQueue;
    dispatchQueue = dispatch_queue_create(kCaptureSessionQueue, NULL);
    [captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatchQueue];
    [captureMetadataOutput setMetadataObjectTypes:[NSArray arrayWithObject:AVMetadataObjectTypeQRCode]];
    
    // Set our preview View
    self.videoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.captureSession];
    [self.videoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [self.videoPreviewLayer setFrame:self.qrPreviewView.layer.bounds];
    [self.qrPreviewView.layer addSublayer:self.videoPreviewLayer];
    
    // Hide Administrator View and Nav Bar
    self.dashboardView.hidden = YES;
    self.navBar.hidden = YES;
    
    // Show Preview View
    self.qrPreviewView.hidden = NO;
    
    // Start Capturing Session
    [self.captureSession startRunning];
    
    return YES;
}

- (NSURL *)checkIfDataIsURL:(NSString *)possibleURLString {
    
    NSURL *possibleURL = [NSURL URLWithString:possibleURLString];
    if (possibleURL && possibleURL.scheme && possibleURL.host){
        return possibleURL;
    } else {
        return nil;
    }
    
}

- (void)stopReading {
    [self.captureSession stopRunning];
    self.captureSession = nil;
    
    [self.videoPreviewLayer removeFromSuperlayer];
    
    self.navBar.hidden = NO;
}

- (void)loadBeepSound {
    NSString *beepFilePath = [[NSBundle mainBundle] pathForResource:@"beep" ofType:@"mp3"];
    NSURL *beepURL = [NSURL URLWithString:beepFilePath];
    NSError *error;
    
    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:beepURL error:&error];
    if (error) {
        NSLog(@"Could not play beep file.");
        NSLog(@"%@", [error localizedDescription]);
    }
    else{
        [self.audioPlayer prepareToPlay];
    }
}

- (void)restoreDashboard {
    
    // Hide and Show Views
    self.chartView.hidden = YES;
    self.qrPreviewView.hidden = YES;
    self.dashboardView.hidden = NO;
    self.navBar.hidden = NO;
}

- (void)registerElementWithDictionary:(NSDictionary *)elementDic {
    
    // Restore Dashboard
    [self restoreDashboard];
    
    // Base URL for Firebase
    Firebase *dbRef = [[Firebase alloc] initWithUrl:@"https://smarttree.firebaseio.com"];
    
    
    if([elementDic[@"element"] isEqualToString:@"tree"]) {
        
        if(![self.treeArray containsObject:elementDic[@"uid"]]){
            
            NSString *treePartialPath = [elementDic[@"element"] stringByAppendingString:[@"/" stringByAppendingString:elementDic[@"uid"]]];
            NSString *treePath = [treePartialPath stringByAppendingString:@"/maintenance"];
            // Create a reference to a Firebase database URL
            Firebase *treeUrl = [dbRef childByAppendingPath: treePath];
            Firebase *treeAutoID = [treeUrl childByAutoId];
            
            NSNumber *timeInterval = [NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970]];
            // Write data to Firebase in Dictionary
            NSDictionary *treeEventDict = @
            {
                @"date": timeInterval,
                @"latitude": [NSNumber numberWithDouble:self.userCoordinate.latitude],
                @"longitude": [NSNumber numberWithDouble:self.userCoordinate.longitude],
                @"eventType": @"Installation",
                @"type": elementDic[@"type"]
            };
            
            // Post to Firebase Database
            [treeAutoID setValue: treeEventDict];
            
            /*-------VISIBILITY---------*/
            
            // Show Registration in Dashboard and Enable Sensor and Maintenance
            self.treeButton.hidden = NO;
            self.sensorAddButton.enabled = YES;
            self.treeMaintButton.enabled = YES;
            return;
        } else {
            NSString *message = [NSString stringWithFormat:@"Tree %@ is already registered, try a different tree.", elementDic[@"uid"]];
            [self.help showAlertOnViewController:self withTitle:@"Tree already registered" andMessage:message andActionTitle:@"Dismiss"];
        }
    }
    
    if(self.treeURLs.count > 0) {
        if([elementDic[@"element"] isEqualToString:@"sensor"]) {
            
            if(![self.treeSensorsArray containsObject:elementDic[@"uid"]]){
                
                NSString *sensorOrigin = [self.treeURLs[self.treeArray[0]] stringByAppendingString:@"/sensor"];
                
                NSString *sensorPartialPath = [sensorOrigin stringByAppendingString:[@"/" stringByAppendingString:elementDic[@"uid"]]];
                NSString *sensorPath = [sensorPartialPath stringByAppendingString:@"/maintenance"];
                
                // Create a reference to a Firebase database URL
                Firebase *sensorUrl = [dbRef childByAppendingPath: sensorPath];
                Firebase *sensorAutoID = [sensorUrl childByAutoId];
                
                NSNumber *timeInterval = [NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970]];
                // Write data to Firebase in Dictionary
                NSDictionary *sensorEventDict = @
                {
                    @"date": timeInterval,
                    @"latitude": [NSNumber numberWithDouble:self.userCoordinate.latitude],
                    @"longitude": [NSNumber numberWithDouble:self.userCoordinate.longitude],
                    @"eventType": @"Installation",
                    @"type": elementDic[@"type"]
                };
                
                // Post to Firebase Database
                [sensorAutoID setValue: sensorEventDict];
                
                /*-------VISIBILITY---------*/
                
                // Show Registration in Dashboard and Enable Sensor and Maintenance
                self.treeButton.hidden = NO;
                self.sensorAddButton.enabled = YES;
                self.treeMaintButton.enabled = YES;
                if([elementDic[@"type"] isEqualToString:@"bulb"]) {
                    self.bulbButton.hidden = NO;
                    self.sensorMaintButton.enabled = YES;
                } else {
                    self.speakerButton.hidden = NO;
                    self.sensorMaintButton.enabled = YES;
                }
                return;
            } else {
                
                NSString *message = [NSString stringWithFormat:@"Sensor %@ is already registered, try a different Sensor.", elementDic[@"uid"]];
                [self.help showAlertOnViewController:self withTitle:@"Sensor already registered" andMessage:message andActionTitle:@"Dismiss"];
                
                // Start Reading QRCodes
                [self startReadingQRCodes];
            }
        }
    } else {
        NSString *message = [NSString stringWithFormat:@"Register a Tree First."];
        [self.help showAlertOnViewController:self withTitle:@"No Trees registered" andMessage:message andActionTitle:@"Dismiss"];
    }
}

- (void) sensorRemovalAction:(NSDictionary *)elementDic {
    // Base URL for Firebase
    NSString *baseRef = @"https://smarttree.firebaseio.com/";
    NSString *sensorOrigin = [self.treeURLs[self.treeArray[0]] stringByAppendingString:@"/sensor/"];
    NSString *sensorPath = [sensorOrigin stringByAppendingString:elementDic[@"uid"]];
    NSString *fullPath = [baseRef stringByAppendingString:sensorPath];
    
    Firebase *dbRef = [[Firebase alloc] initWithUrl:fullPath];
    
    [[dbRef queryOrderedByChild:elementDic[@"uid"]]
     observeEventType:FEventTypeChildAdded withBlock:^(FDataSnapshot *snapshot) {
         [dbRef removeAllObservers];
         [self.mainRef removeAllObservers];
         NSString *baseRefRemove = @"https://smarttree.firebaseio.com/remove/";
         NSString *sensorOriginRemove = [baseRefRemove stringByAppendingString:elementDic[@"uid"]];
         Firebase *dbRefRemove = [[Firebase alloc] initWithUrl:sensorOriginRemove];
         
         [dbRefRemove setValue:snapshot.value];
         [dbRef setValue:nil];
         
         // Restore Dashboard
         [self restoreDashboard];
         
         // Remove Icon
         NSRange range = [elementDic[@"uid"] rangeOfString:@"^SEB[0-9]" options:NSRegularExpressionSearch];
         if(range.location != NSNotFound){
             self.bulbLabel.text = @"";
             self.bulbButton.hidden = YES;
         } else {
             self.speakerLabel.text = @"";
             self.speakerButton.hidden = YES;
         }
         
         // Reset Flags
         self.maintenanceFlag = NO;
         [self.treeSensorsArray removeObject:elementDic[@"uid"]];
         [self listenToFirebaseDbForTreeEvents];
     }];
}

- (void) sensorReplaceAction:(NSDictionary *)elementDic {
    [self sensorRemovalAction:elementDic];
    
    // Create AlertController
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Now Scan New Sensor..."
                                          message:@"Scan New Sensor to Replace Removed One."
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    // Create Action for AlertController
    UIAlertAction *dissmisAction = [UIAlertAction actionWithTitle:@"Dismiss"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction *actionPerformed){
                                                              [self startReadingQRCodes];
                                                          }];
    // Add Action to AlertController
    [alertController addAction:dissmisAction];
    
    // Present Alert Controller
    [self presentViewController:alertController animated:YES completion:nil];
    
}

- (void) treeRemovalAction:(NSDictionary *)elementDic {
    // Base URL for Firebase
    NSString *baseRef = @"https://smarttree.firebaseio.com/tree/";
    NSString *treePath = [baseRef stringByAppendingString:elementDic[@"uid"]];
    
    Firebase *dbRef = [[Firebase alloc] initWithUrl:treePath];
    
    [[dbRef queryOrderedByChild:elementDic[@"uid"]]
     observeEventType:FEventTypeChildAdded withBlock:^(FDataSnapshot *snapshot) {
         [dbRef removeAllObservers];
         [self.mainRef removeAllObservers];
         NSString *baseRefRemove = @"https://smarttree.firebaseio.com/remove/";
         NSString *sensorOriginRemove = [baseRefRemove stringByAppendingString:elementDic[@"uid"]];
         Firebase *dbRefRemove = [[Firebase alloc] initWithUrl:sensorOriginRemove];
         
         [dbRefRemove setValue:snapshot.value];
         [dbRef setValue:nil];
         
         // Restore Dashboard
         [self restoreDashboard];
         
         // Remove Icon
         NSRange range = [elementDic[@"uid"] rangeOfString:@"^S3DT[0-9]" options:NSRegularExpressionSearch];
         if(range.location != NSNotFound){
             self.treeLabel.text = @"";
             self.treeButton.hidden = YES;
             self.bulbButton.hidden = YES;
             self.bulbLabel.text = @"";
             self.speakerButton.hidden = YES;
             self.speakerLabel.text = @"";
         }
         
         // Reset Flags
         self.maintenanceFlag = NO;
         [self.treeArray removeObject:elementDic[@"uid"]];
         [self.treeSensorsArray removeAllObjects];
         [self listenToFirebaseDbForTreeEvents];
     }];
}

- (void) treeReplaceAction:(NSDictionary *)elementDic {
    [self treeRemovalAction:elementDic];
    
    // Create AlertController
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Now Scan New Tree..."
                                          message:@"Scan New Tree to Replace Removed One."
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    // Create Action for AlertController
    UIAlertAction *dissmisAction = [UIAlertAction actionWithTitle:@"Dismiss"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction *actionPerformed){
                                                              [self startReadingQRCodes];
                                                          }];
    // Add Action to AlertController
    [alertController addAction:dissmisAction];
    
    // Present Alert Controller
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark -  Action Sheet Methods
- (IBAction)sensorMaintenancePressed:(id)sender {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Sensor Maintenance"
                                                                   message:@"Select an Action"
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"Remove"
                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                              NSLog(@"You pressed Remove");
                                                              [self removeSensorAction];
                                                          }];
    
    UIAlertAction *secondAction = [UIAlertAction actionWithTitle:@"Replace"
                                                           style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                               NSLog(@"You pressed Replace");
                                                               [self replaceSensorAction];
                                                           }];
    
    [alert addAction:firstAction];
    [alert addAction:secondAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)removeSensorAction {
    
    // Set Flag for Sensor Remova
    self.maintenanceFlag = YES;
    
    // Set Action in enum
    self.maintenaceAction = SensorRemoval;
    
    // Start Reading
    [self startReadingQRCodes];
}

- (void)replaceSensorAction {
    
    // Set Flag for Sensor Remova
    self.maintenanceFlag = YES;
    
    // Set Action in enum
    self.maintenaceAction = SensorReplace;
    
    // Start Reading
    [self startReadingQRCodes];
}

- (IBAction)treeMaintenancePressed:(UIButton *)sender {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Tree Maintenance"
                                                                   message:@"Select an Action"
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"Remove"
                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                              NSLog(@"You pressed Remove");
                                                              [self removeTreeAction];
                                                          }];
    
    UIAlertAction *secondAction = [UIAlertAction actionWithTitle:@"Replace"
                                                           style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                               NSLog(@"You pressed Replace");
                                                               [self replaceTreeAction];
                                                           }];
    
    [alert addAction:firstAction];
    [alert addAction:secondAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)removeTreeAction {
    
    // Set Flag for Sensor Remova
    self.maintenanceFlag = YES;
    
    // Set Action in enum
    self.maintenaceAction = TreeRemoval;
    
    // Start Reading
    [self startReadingQRCodes];
}

- (void)replaceTreeAction {
    
    // Set Flag for Sensor Remova
    self.maintenanceFlag = YES;
    
    // Set Action in enum
    self.maintenaceAction = TreeReplace;
    
    // Start Reading
    [self startReadingQRCodes];
}

- (IBAction)treeChartPressed:(UIButton *)sender {
    
    [self parseDataToElements];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Sensor Data"
                                                                   message:@"Select an Chart"
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"Light Color"
                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                              NSLog(@"You pressed Light Color");
                                                              [self lightColorGraph];
                                                          }];
    
    UIAlertAction *secondAction = [UIAlertAction actionWithTitle:@"Light Intensity"
                                                           style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                               NSLog(@"You pressed Light Intensity");
                                                               [self lightIntensityGraph];
                                                           }];
    
    UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"Music Changed"
                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                              NSLog(@"You pressed Music Changed");
                                                              [self musicChangedGraph];
                                                          }];
    
    [alert addAction:firstAction];
    [alert addAction:secondAction];
    [alert addAction:thirdAction];
    
    [self presentViewController:alert animated:YES completion:nil];

}

- (IBAction)dashboardPressed:(UIButton *)sender {
    
    [self restoreDashboard];

}

- (void)lightColorGraph {
    
    NSMutableArray *colorValues = [[NSMutableArray alloc]init];
    
    for(NSDictionary *interaction in self.bulbDataArray){
        [colorValues addObject:interaction[@"hue"]];
    }
    
    // Reload the graph
    [self.graphDataArray removeAllObjects];
    self.graphDataArray = colorValues;
    self.chartView.averageLine.enableAverageLine = YES;
    [self.chartView reloadGraph];
    
    // Hide and Show Views
    self.chartView.hidden = NO;
    self.qrPreviewView.hidden = YES;
    self.dashboardView.hidden = YES;

}

- (void)lightIntensityGraph {
    
    NSMutableArray *intensityValues = [[NSMutableArray alloc]init];
    
    for(NSDictionary *interaction in self.bulbDataArray){
        [intensityValues addObject:interaction[@"sat"]];
    }
    
    // Reload the graph
    [self.graphDataArray removeAllObjects];
    self.graphDataArray = intensityValues;
    self.chartView.averageLine.enableAverageLine = YES;
    [self.chartView reloadGraph];
    
    // Hide and Show Views
    self.chartView.hidden = NO;
    self.qrPreviewView.hidden = YES;
    self.dashboardView.hidden = YES;
    
}
- (void)musicChangedGraph {
    
    NSMutableArray *musicValues = [[NSMutableArray alloc]init];

    for(NSDictionary *interaction in self.speakerDataArray){
        if([interaction[@"musicSelection"] isEqualToString:@"Clearday"]) {
            [musicValues addObject:@1];
        } else if([interaction[@"musicSelection"] isEqualToString:@"Littleidea"]) {
            [musicValues addObject:@2];
        } else if([interaction[@"musicSelection"] isEqualToString:@"Energy"]) {
            [musicValues addObject:@3];
        }
    }
    
    // Reload the graph
    [self.graphDataArray removeAllObjects];
    self.graphDataArray = musicValues;
    self.chartView.averageLine.enableAverageLine = NO;
    [self.chartView reloadGraph];
    
    // Hide and Show Views
    self.chartView.hidden = NO;
    self.qrPreviewView.hidden = YES;
    self.dashboardView.hidden = YES;
    
}
- (void)parseDataToElements {
    
    // Get Tree Data
    [self.treeDataArray removeAllObjects];
    
    if([self.treeDataDict.allKeys containsObject:@"maintenance"]){
        NSDictionary *maintenance = self.treeDataDict[@"maintenance"];
        NSDictionary *autoID = maintenance[[maintenance.allKeys objectAtIndex:0]];
        [self.treeDataArray addObject:autoID];
    }
    
    // Get Sensor Data
    
    [self.bulbDataArray removeAllObjects];
    [self.speakerDataArray removeAllObjects];
    if([self.treeDataDict.allKeys containsObject:@"sensor"]){
        NSDictionary *sensorsDict = self.treeDataDict[@"sensor"];
        NSArray *sensorKeyArray = sensorsDict.allKeys;
        for(NSString *key in sensorKeyArray) {
            NSDictionary *sensors = sensorsDict[key];
            if(sensors.allKeys.count >1) {  // Got Iteractions
                for(NSString *actions in sensors.allKeys){
                    if(![actions isEqualToString:@"maintenance"]){
                        NSDictionary *actionAutoIDArray = sensors[actions];
                        for(NSString *aAutoID in actionAutoIDArray.allKeys){
                            NSDictionary *data = actionAutoIDArray[aAutoID];
                            if(data.allKeys.count > 2){
                                //Light
                                [self.bulbDataArray addObject:data];
                            } else {
                                [self.speakerDataArray addObject:data];
                            }
                        }
                    }
                }
            }
        }

    }

    NSLog(@"Finish");
}

#pragma mark -  AVCaptureMetadataOutputObjectDelegate Methods
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection {
    
    if (metadataObjects != nil && [metadataObjects count] > 0) {
        AVMetadataMachineReadableCodeObject *metadataObj = [metadataObjects objectAtIndex:0];
        if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeQRCode]) {
            
            NSURL *urlFromMetadata = [self checkIfDataIsURL:[metadataObj stringValue]];
            if(urlFromMetadata) {
                [self performSelectorOnMainThread:@selector(stopReading) withObject:nil waitUntilDone:NO];
                // Nothing to do, just accept text
            } else {
                
                [self performSelectorOnMainThread:@selector(stopReading) withObject:nil waitUntilDone:NO];
                
                NSData *jsonData = [[metadataObj stringValue] dataUsingEncoding:NSUTF8StringEncoding];
                NSError *e;
                NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&e];
                
                if(self.maintenanceFlag) {
                    [self sortMaintenanceActions:dict];
                } else {
                    [self performSelectorOnMainThread:@selector(registerElementWithDictionary:) withObject:dict waitUntilDone:NO];
                }

            }
            
            if (self.audioPlayer) {
                [self.audioPlayer play];
            }
            
        }
    }
}

- (void)sortMaintenanceActions:(NSDictionary *)elementDic {
    switch (self.maintenaceAction) {
        case SensorRemoval:
            [self sensorRemovalAction:elementDic];
            break;
        case SensorReplace:
            [self sensorReplaceAction:elementDic];
            break;
        case TreeRemoval:
            [self treeRemovalAction:elementDic];
            break;
        case TreeReplace:
            [self treeReplaceAction:elementDic];
            break;
        default:
            break;
    }
}

#pragma mark - CLLocationManagerDelegate Methods
- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    
    // User Authorized app to use Location Services
    if(status != kCLAuthorizationStatusNotDetermined)
        [self.locationManager startUpdatingLocation];
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    self.currentLocation = locations.lastObject;
    self.userCoordinate = self.currentLocation.coordinate;
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"Error Locating user Location Manager: %@", error);
    if([error.domain isEqualToString: kCLErrorDomain])
        NSLog(@"Error Updating Location");
    else
        NSLog(@"Other Location Error");
}

#pragma mark - BEMSimpleLineGraphDelegate and Datasource Methods
- (NSInteger)numberOfPointsInLineGraph:(BEMSimpleLineGraphView *)graph {
    return self.graphDataArray.count; // Number of points in the graph.
}

- (CGFloat)lineGraph:(BEMSimpleLineGraphView *)graph valueForPointAtIndex:(NSInteger)index {
    return [self.graphDataArray[index] doubleValue]; // The value of the point on the Y-Axis for the index.
}

#pragma mark - SimpleLineGraph Delegate

- (NSInteger)numberOfGapsBetweenLabelsOnLineGraph:(BEMSimpleLineGraphView *)graph {
    return 2;
}

//- (NSString *)lineGraph:(BEMSimpleLineGraphView *)graph labelOnXAxisForIndex:(NSInteger)index {
//    
//    NSString *label = [self labelForDateAtIndex:index];
//    return [label stringByReplacingOccurrencesOfString:@" " withString:@"\n"];
//}

- (NSString *)labelForDateAtIndex:(NSInteger)index {
    NSDate *date = self.arrayOfDates[index];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.dateFormat = @"MM/dd";
    NSString *label = [df stringFromDate:date];
    return label;
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
