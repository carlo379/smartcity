//
//  ShareViewController.h
//  SmartCity
//
//  Created by Carlos Martinez on 3/17/16.
//  Copyright © 2016 Carlos Martinez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShareViewController : UIViewController

@property (strong, nonatomic) NSURL *mediaURL;

@end
