//
//  ShareViewController.m
//  SmartCity
//
//  Created by Carlos Martinez on 3/17/16.
//  Copyright © 2016 Carlos Martinez. All rights reserved.
//

#import "ShareViewController.h"
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "Firebase.h"
#import "FirebaseLoginViewController.h"
#import "LoginViewController.h"

@import AVFoundation;
@import AVKit;

#pragma mark - Interface
@interface ShareViewController()<MFMailComposeViewControllerDelegate, FBSDKSharingDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

#pragma mark Properties
@property (weak, nonatomic) IBOutlet UIButton *facebookButton;
@property (weak, nonatomic) IBOutlet UIButton *emailButton;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIView *videoPlayerContainer;
@property (weak, nonatomic) IBOutlet UIButton *mediaPickerButton;
@property (strong, nonatomic) UIImage *chosenImage;
@property (strong, nonatomic) NSURL *chosenVideoPath;
@property (strong, nonatomic) FirebaseLoginViewController *loginViewController;
@property (strong, nonatomic) AVPlayerViewController *playerViewController;
@end

#pragma mark - Implementation
@implementation ShareViewController

#pragma mark - View Life Cycle Methods
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // create an AVPlayer
    self.playerViewController = [[AVPlayerViewController alloc]init];
    
    NSString *extension = [self.mediaURL pathExtension];

    // Check if media is Video or Photo
    if([extension isEqualToString:@"mov"]){
        
        // show videoContainer
        self.videoPlayerContainer.hidden = YES;
        self.imageView.hidden = YES;
        self.mediaPickerButton.hidden = NO;
        
    } else if([extension isEqualToString:@"jpg"]) {
        
        self.imageView.hidden = NO;
        self.videoPlayerContainer.hidden = YES;
        self.mediaPickerButton.hidden = YES;
        self.imageView.image = [UIImage imageWithContentsOfFile:[self.mediaURL path]];
        self.chosenImage = [UIImage imageWithContentsOfFile:[self.mediaURL path]];
        
    } else {
        self.videoPlayerContainer.hidden = YES;
        self.imageView.hidden = YES;
        self.mediaPickerButton.hidden = NO;
    }
    
    // Login Setup with Firebase
    Firebase *firebaseRef = [[Firebase alloc] initWithUrl:@"https://smarttree.firebaseio.com/"];
    
    self.loginViewController = [[FirebaseLoginViewController alloc] initWithRef:firebaseRef];
    [self.loginViewController enableProvider:FAuthProviderFacebook];
    [self.loginViewController enableProvider:FAuthProviderPassword];

}

#pragma mark - Login Sequence Methods
- (void)login {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *user = [defaults objectForKey:@"LoggedUser"];
    
    if (!user) {
        NSString * storyboardName = @"Main";
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
        UIViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
        [self presentViewController:vc animated:YES completion:nil];
    }
}

- (void)logout {

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:nil forKey:@"LoggedUser"];
}

- (void) displayContentController: (UIViewController*) content {
    
    // Content View COntroller for Video Playback
    [self addChildViewController:content];
    content.view.frame = self.videoPlayerContainer.frame;
    [self.view addSubview:content.view];
    [content didMoveToParentViewController:self];
}

#pragma mark - IBActions
- (IBAction)cancelButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)facebookPressed:(UIButton *)sender {
    // Check if user is Logged
    [self login];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *user = [defaults objectForKey:@"LoggedUser"];
    
    if(!user)
        return;
    
    
    // Check what type of media they want to share (Video or Photo)
    if(self.chosenImage || (!self.chosenVideoPath && !self.chosenImage) ) {
        
        if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
        {
            SLComposeViewController *facebookPost = [SLComposeViewController
                                                     composeViewControllerForServiceType: SLServiceTypeFacebook];
            [facebookPost setInitialText:@"Text You want to Share"];
            
            if(self.chosenImage)
                [facebookPost addImage:self.chosenImage];
            
            [self presentViewController:facebookPost animated:YES completion:nil];
            [facebookPost setCompletionHandler:^(SLComposeViewControllerResult result) {
                switch (result) {
                    case SLComposeViewControllerResultCancelled:
                        NSLog(@"Post Canceled");
                        break;
                    case SLComposeViewControllerResultDone:
                        NSLog(@"Post Sucessful");
                        break;
                    default:
                        break;
                }
                [self dismissViewControllerAnimated:YES completion:nil];
            }];
        }
        
    } else if (self.chosenVideoPath) {
        
        
        FBSDKShareVideo *video = [[FBSDKShareVideo alloc] init];
        video.videoURL = self.chosenVideoPath;
        FBSDKShareVideoContent *videoContent = [[FBSDKShareVideoContent alloc] init];
        videoContent.video = video;
        
        //[FBSDKMessageDialog showWithContent:content delegate:nil];
        
        FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
        content.contentTitle = @"First Post";
        content.contentDescription = @"This is the Description";
        
        FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
        dialog.delegate = self;
        dialog.fromViewController = self;
        dialog.shareContent = videoContent;
        dialog.mode = FBSDKShareDialogModeAutomatic;
        [dialog show];
        
        
        [FBSDKShareVideo videoWithVideoURL:self.mediaURL];

    }
    
}

- (IBAction)emailPressed:(UIButton *)sender {
    // Check if user is Logged
    
    if(!self.mediaURL) return;
    
    [self login];

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *user = [defaults objectForKey:@"LoggedUser"];
    
    if(!user)
        return;
    
    NSString *emailTitle = @"Great Photo";
    NSString *messageBody = @"Hey, check this out!";
    NSArray *toRecipents = [NSArray arrayWithObject:@"carlos.a.martinez@sjsu.edu"];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:NO];
    [mc setToRecipients:toRecipents];
    
    NSData *data = [NSData dataWithContentsOfURL:self.mediaURL];

    
    NSString *extension = [self.mediaURL pathExtension];
    
    // Determine the MIME type
    NSString *mimeType;
    if ([extension isEqualToString:@"jpg"]) {
        mimeType = @"image/jpeg";

    } else if ([extension isEqualToString:@"mov"]) {
        mimeType = @"video/quicktime";
    }
    
    // Add attachment
    [mc addAttachmentData:data mimeType:mimeType fileName:[self.mediaURL lastPathComponent]];
    
    // Present mail view controller on screen
    [self presentViewController:mc animated:YES completion:NULL];
}

- (IBAction)changeSelectMediaPressed:(id)sender {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate=self;
    [picker setSourceType:(UIImagePickerControllerSourceTypePhotoLibrary)];
    picker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, (NSString *) kUTTypeVideo, (NSString *)kUTTypeImage, nil];
    [self presentViewController:picker animated:YES completion:Nil];
    
}

#pragma mark - UIImagePickerDelegate Method
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    NSString *type = [info objectForKey:UIImagePickerControllerMediaType];
    
    if ([type isEqualToString:(NSString *)kUTTypeVideo] || [type isEqualToString:(NSString *)kUTTypeMovie])
    {
        self.chosenVideoPath = [info objectForKey:UIImagePickerControllerReferenceURL];
        self.videoPlayerContainer.hidden = NO;
        self.imageView.hidden = YES;
        self.mediaPickerButton.hidden =YES;
    
        NSURL *tempURL = [info objectForKey:UIImagePickerControllerMediaURL];
        
        self.mediaURL = [NSURL fileURLWithPath:[self stringPathWithURL:tempURL orData:nil andFileName:@"/tempVideo.mov"]];
        self.playerViewController.player = [AVPlayer playerWithURL:self.mediaURL];
        [self displayContentController:self.playerViewController];
    }
    if ([type isEqualToString:(NSString *)kUTTypeImage]) {
        
        self.chosenImage = info[UIImagePickerControllerOriginalImage];
        NSData *jpgData = UIImageJPEGRepresentation(info[UIImagePickerControllerOriginalImage], 1.0);
        self.mediaURL = [NSURL fileURLWithPath:[self stringPathWithURL:nil orData:jpgData andFileName:@"/tempPhoto.jpg"]];
        
        self.imageView.image = self.chosenImage;
        self.videoPlayerContainer.hidden = YES;
        self.imageView.hidden = NO;
        self.mediaPickerButton.hidden =YES;

    }
    
    // Dismisses imagePicker Add image to Button
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark UIImagePicker Convenience Methods
-(NSString *)stringPathWithURL:(NSURL *)mediaURL orData:(NSData *)data andFileName:(NSString *)fileName {
    
    if(!data){
        data = [NSData dataWithContentsOfURL:mediaURL];
    }
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *tempPath = [documentsDirectory stringByAppendingFormat:@"%@", fileName];
    if([data writeToFile:tempPath atomically:NO]) {
        return tempPath;
    } else {
        // TO DO: create Alert to User to let them know of failure
        return nil;
    }
}

#pragma mark - MailComposerDelegate Method
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - Facebook Share Delegate Methods
- (void)sharerDidCancel:(id<FBSDKSharing>)sharer {
    NSLog(@"Post Canceled");

}

- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error {
    NSLog(@"Post Canceled");

}

- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results {
    NSLog(@"Post Canceled");

}

#pragma mark - Navigation Segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    // Segue to Edit Shoe for Container View
    if ([segue.identifier isEqualToString: @"showMovieSegue"]) {
        
        self.playerViewController = segue.destinationViewController;
        
        // setup player to movie
        self.playerViewController.player = [AVPlayer playerWithURL:self.mediaURL];
        
    }
}

@end
