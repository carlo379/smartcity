//
//  SignupViewController.m
//  SmartCity
//
//  Created by Carlos Martinez on 3/28/16.
//  Copyright © 2016 Carlos Martinez. All rights reserved.
//

#import "SignupViewController.h"
#import "Firebase.h"
#import "Constants.h"

#pragma mark - Interface
@interface SignupViewController ()<UITextFieldDelegate>

#pragma mark Properties
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *signupSpinner;
@property (weak, nonatomic) IBOutlet UIImageView *nameErrorImageView;
@property (weak, nonatomic) IBOutlet UIImageView *emailErrorImageView;
@property (weak, nonatomic) IBOutlet UIImageView *passwordErrorImageView;

@end

#pragma mark Implementation
@implementation SignupViewController

#pragma mark - View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBActions
- (IBAction)registerPressed:(UIButton *)sender {
    
    // Start Spinner to signal user of Start of Login sequence
    [self.signupSpinner startAnimating];
    
    // Check Textfields
    if(![self checkTextFields]){
        
        // Stop Spinner Animation
        [self.signupSpinner stopAnimating];
        
        return;
    }
    
    Firebase *ref = [[Firebase alloc] initWithUrl:@"https://smarttree.firebaseio.com"];
    [ref createUser:self.emailTextField.text password:self.passwordTextField.text withValueCompletionBlock:^(NSError *error, NSDictionary *result) {
    if (error) {
        // There was an error creating the account
        NSLog(@"Error Creating Account");
    } else {
        NSString *uid = [result objectForKey:@"uid"];
        NSLog(@"Successfully created user account with uid: %@", uid);
        
        // Save Newly created user reference in NSDefaults
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setValue:self.emailTextField.text forKey:@"LoggedUser"];
        
        if([uid isEqualToString:kAdministratorUID]){
            
            NSString * storyboardName = @"Main";
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
            UIViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"AdministratorViewController"];
            [self presentViewController:vc animated:YES completion:nil];
        }

        
        // Dismiss View
        [self dismissViewControllerAnimated:YES completion:nil];
        [self.navigationController popViewControllerAnimated:YES];
    }
        // Stop Spinner Animation
        [self.signupSpinner stopAnimating];

}];
}

- (IBAction)loginPressed:(UIButton *)sender {
    
    // Present Login View Controller
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)checkTextFields {
    
    BOOL result = TRUE;
    
    if([self.nameTextField.text isEqualToString:@""]){
        self.nameErrorImageView.hidden = NO;
        result = FALSE;
    }
    
    if([self.emailTextField.text isEqualToString:@""]){
        self.emailErrorImageView.hidden = NO;
        result = FALSE;
    } else {
        if(![self NSStringIsValidEmail:self.emailTextField.text]){
            self.emailErrorImageView.hidden = NO;
            result = FALSE;
        }
    }
    
    if([self.passwordTextField.text isEqualToString:@""]){
        self.passwordErrorImageView.hidden = NO;
        result = FALSE;
    }
    
    return result;
}

#pragma mark - UITextFieldDelegate Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if(textField == self.nameTextField) {
        self.nameErrorImageView.hidden = YES;
    }
    
    if(textField == self.emailTextField) {
        self.emailErrorImageView.hidden = YES;
    }
    
    if(textField == self.passwordTextField) {
        self.passwordErrorImageView.hidden = YES;
    }
    
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

@end
