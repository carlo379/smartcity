//
//  AudioRecorderViewController.m
//  SmartCity
//
//  Created by Carlos Martinez on 3/13/16.
//  Copyright © 2016 Carlos Martinez. All rights reserved.
//

#import "AudioRecorderViewController.h"

@interface AudioRecorderViewController ()

{
    AVAudioRecorder *recorder;
    AVAudioPlayer *player;
}
@property (weak, nonatomic) IBOutlet UIButton *recCircleImage;
@property (weak, nonatomic) IBOutlet UIButton *recordStopButton;
@property (weak, nonatomic) IBOutlet UIButton *playButton;

@end

@implementation AudioRecorderViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.playButton.hidden = YES;
    self.recCircleImage.hidden = YES;
    
    // Set the audio file
    NSArray *pathComponents = [NSArray arrayWithObjects:
                               [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],
                               @"MyAudioMemo.m4a",
                               nil];
    NSURL *outputFileURL = [NSURL fileURLWithPathComponents:pathComponents];
    
    // Setup audio session
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    [session overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:nil];
    
    
    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
    
    [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
    
    recorder = [[AVAudioRecorder alloc] initWithURL:outputFileURL settings:recordSetting error:nil];
    recorder.delegate = self;
    recorder.meteringEnabled = YES;
    [recorder prepareToRecord];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)recordStopPressed:(UIButton *)sender {
    if (player.playing) {
        [player stop];
    }
    
    if (!recorder.recording) {
        AVAudioSession *session = [AVAudioSession sharedInstance];
        [session setActive:YES error:nil];
        
        // Start recording
        [recorder record];
        [self.recordStopButton setTitle:@"Stop" forState:UIControlStateNormal];
        self.recCircleImage.hidden = NO;
        
    } else {
        
        // Pause recording
        [recorder stop];
        [self.recordStopButton setTitle:@"Record" forState:UIControlStateNormal];
        
        AVAudioSession *audioSession = [AVAudioSession sharedInstance];
        [audioSession setActive:NO error:nil];
        self.recCircleImage.hidden = YES;

    }
    
    self.playButton.hidden = NO;
}

- (IBAction)playPressed:(UIButton *)sender {
    
    if (!recorder.recording){
        player = [[AVAudioPlayer alloc] initWithContentsOfURL:recorder.url error:nil];
        [player setDelegate:self];
        [player play];
    }
}

- (IBAction)donePressed:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - AVAudioRecorderDelegate

- (void) audioRecorderDidFinishRecording:(AVAudioRecorder *)avrecorder successfully:(BOOL)flag{
    [self.recordStopButton setTitle:@"Record" forState:UIControlStateNormal];
    self.playButton.hidden = NO;
}

#pragma mark - AVAudioPlayerDelegate

- (void) audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Done"
                                                    message: @"Finish playing the recording!"
                                                   delegate: nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}


@end