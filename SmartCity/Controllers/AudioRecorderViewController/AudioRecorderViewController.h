//
//  AudioRecorderViewController.h
//  SmartCity
//
//  Created by Carlos Martinez on 3/13/16.
//  Copyright © 2016 Carlos Martinez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface AudioRecorderViewController : UIViewController<AVAudioRecorderDelegate,AVAudioPlayerDelegate>
@end

