//
//  DirectionsTableViewController.h
//  SmartCity
//
//  Created by Carlos Martinez on 3/1/16.
//  Copyright © 2016 Carlos Martinez. All rights reserved.
//

#import <UIKit/UIKit.h>

// Protocol to notify when the "...search" button was pressed or when a user made a selection
@protocol DirectionsTableViewControllerDelegate <NSObject>
- (MKRoute *)getRouteInformation;
@end

@interface DirectionsTableViewController : UITableViewController

// delegate property for protocol set-up.
@property (nonatomic, weak) id <DirectionsTableViewControllerDelegate> delegate;

// Property to set the route
@property (nonatomic, strong) MKRoute *route;

@end
