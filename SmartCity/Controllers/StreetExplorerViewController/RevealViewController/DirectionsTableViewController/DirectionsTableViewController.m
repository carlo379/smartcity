//
//  DirectionsTableViewController.m
//  SmartCity
//
//  Created by Carlos Martinez on 3/1/16.
//  Copyright © 2016 Carlos Martinez. All rights reserved.
//
@import MapKit;
#import "DirectionsTableViewController.h"
#import "StreetExplorerViewController.h"

@interface DirectionsTableViewController()

@end

@implementation DirectionsTableViewController
#pragma mark - View Life Cycle Methods
- (void) viewDidLoad {
    [super viewDidLoad];
    
    //Set the color of the navigation bar text
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    // Signal controller that need to update Status Bar for change in color
    [self setNeedsStatusBarAppearanceUpdate];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.route = [self.delegate getRouteInformation];
    [self.tableView reloadData];
}

#pragma mark - UIViewController Methods Override
- (UIStatusBarStyle)preferredStatusBarStyle {
    // Update color/style of StatusBar
    return UIStatusBarStyleLightContent;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return self.route.steps.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"stepsCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    MKRouteStep *step = [self.route.steps objectAtIndex:indexPath.row];
    
    cell.textLabel.text = step.instructions;
    
    return cell;
}

@end
