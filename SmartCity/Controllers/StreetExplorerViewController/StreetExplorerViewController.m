//
//  StreetExplorerViewController.m
//  SmartCity
//
//  Created by Carlos Martinez on 2/25/16.
//  Copyright © 2016 Carlos Martinez. All rights reserved.
//

@import CloudKit;
@import MobileCoreServices;
@import AVFoundation;
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import <QuartzCore/QuartzCore.h>
#import "SWRevealViewController.h"
#import "UIImage+Extensions.h"
#import "StreetExplorerViewController.h"
#import "AppDelegate.h"
#import "DirectionsTableViewController.h"
#import "MapViewAnnotation.h"
#import "CustomAnnotationView.h"
#import "HelpMethods.h"
#import "ShareViewController.h"
#import "Constants.h"

#pragma mark - Constants
float const kNavBarHideTime = 1.0;
float const kSearchBarVisible = 1.0;
float const kSearchBarNotVisible = 0.0;
float const knavBarVerticalSpaceDefault = 0.0;
int   const kNavBarHeight = 60;
int   const kDefaultNavBarVerticalSpaceConst = 0;
static NSString * const kAlertTitle = @"Search Error";
static NSString * const kAlertMessage = @"An error ocurred while searching location. Please try again later.";
static NSString * const kActionTitle = @"Dismiss";
typedef NS_ENUM(NSUInteger, ActionButtonTag) {
    kMapButtonTag = 1011,  // Equivalent to the Tag Number Assigned to the Button
    kQRCodeButtonTag,
    kPictureButtonTag
};
typedef NS_ENUM(NSUInteger, BarVisualState){
    kHide,
    kShow
};

#pragma mark - Interface
@interface StreetExplorerViewController () <DirectionsTableViewControllerDelegate, AVCaptureMetadataOutputObjectsDelegate,CLLocationManagerDelegate,MKMapViewDelegate,UITextViewDelegate,UISearchBarDelegate,UIAlertViewDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,UIGestureRecognizerDelegate,UIWebViewDelegate,UIImagePickerControllerDelegate>
{
#pragma mark Flags
    BOOL firstMapMove;
    BOOL _isReading;
}

#pragma mark Properties
// IBOutlets from view
@property (weak, nonatomic) IBOutlet UIView *qrPreviewView;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLB;// User Discoverability Properties
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *userSpinner;
@property (weak, nonatomic) IBOutlet UIButton *tryAgainBT;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *navBar;
@property (weak, nonatomic) IBOutlet UIButton *locationBT;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *navBarVerticalSpaceConst;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mapViewHeightConst;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *webViewSpinner;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
// Properties of Class
@property (strong, nonatomic) MKMapItem *selectedItem;
@property (strong, nonatomic) AVCaptureSession *captureSession;
@property (strong, nonatomic) AVCaptureVideoPreviewLayer *videoPreviewLayer;
@property (strong, nonatomic) AVAudioPlayer *audioPlayer;
@property (strong, nonatomic) MKPointAnnotation *pin;
@property (strong, nonatomic) CLLocation *currentLocation;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (retain, nonatomic) NSMutableArray *results;
@property (strong, nonatomic) NSTimer *navBarHideTimer;
@property (strong, nonatomic) NSArray *places;
@property (assign, nonatomic) MKCoordinateRegion boundingRegion;
@property (strong, nonatomic) MKLocalSearch *localSearch;
@property (nonatomic) CLLocationCoordinate2D userCoordinate;
@property (strong, nonatomic) MKRoute *currentRoute;
@property (strong, nonatomic) DirectionsTableViewController *directionsTableViewController;
@property (strong, nonatomic) NSURL *currentMediaURL;
@end

#pragma mark - Implementation
@implementation StreetExplorerViewController

#pragma mark - View Life Cycle Methods
- (void) viewDidLoad {
    [super viewDidLoad];
    
    // Set Map Delegate
    self.mapView.delegate = self;
    
    //Initialize Location Manager
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    self.locationManager.delegate = self;
    
    // Request autorization to get user location
    if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [self.locationManager requestAlwaysAuthorization];
    }
    
    //Set the color of the navigation bar text to White
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    // Set flag to signal first Map Move during initialization
    firstMapMove = YES;
    
    // Set the Right bar button action. When it's tapped, it'll show up the sidebar (Directions to location)
    [self.rightSideMenu addTarget:self.revealViewController action:@selector(rightRevealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    // Set characteristics of location button, that are not available in Interface Builder
    self.locationBT.layer.shadowColor = [UIColor blackColor].CGColor;
    self.locationBT.layer.shadowRadius = 5.f;
    self.locationBT.layer.shadowOffset = CGSizeMake(0.f, 3.f);
    self.locationBT.layer.shadowOpacity = 1.f;
    self.locationBT.layer.masksToBounds = NO;
    
    // Load Sound file
    [self loadBeepSound];
    
    // Set as delegate of WebView
    self.webView.delegate = self;
    
    // Set as delegate of Searchbar
    self.searchBar.delegate = self;
    
    // Signal controller that need to update Status Bar for change in color
    [self setNeedsStatusBarAppearanceUpdate];
        
}

#pragma mark - UIViewController Methods Override
- (UIStatusBarStyle)preferredStatusBarStyle {
    // Update color/style of StatusBar
    return UIStatusBarStyleLightContent;
}

#pragma mark - IBActions
- (IBAction)cityExplorerButtonPressed:(UIButton *)sender {
    
    // Show nav bar after comming from another view.
    [self animateNavBarTo:kShow];
    
    // Set Tag as a 'ActionButtonTag' typedef
    ActionButtonTag actionButtonTag = sender.tag;
    
    // Hide others views that are not going to be used
    [self hideActionViewsExceptViewForActionWithTag:actionButtonTag];
    
    // Start action for current button pressed
    [self startActionWithTag:actionButtonTag];
}

- (IBAction)searchButtonPressed:(UIButton *)sender {
    
    // Toggle between showing and hiding searchbar
    if(self.searchBar.alpha == kSearchBarNotVisible){
        [self animateSearchBarTo:kShow];
        [self.searchBar becomeFirstResponder];
    } else {
        [self animateSearchBarTo:kHide];
        [self.searchBar resignFirstResponder];
    }
}

- (IBAction)refreshLocationPressed:(UIButton *)sender {
    // Refresh User Location
    [self.locationManager startUpdatingLocation];
}

- (IBAction)cameraPressed:(UIButton *)sender {
//    self.imagePickerCoordinator = [IDImagePickerCoordinator new];
//    [self presentViewController:[_imagePickerCoordinator cameraVC] animated:YES completion:nil];
    
    NSString *alertTitle = NSLocalizedString(@"Media Type", @"Title of alert controller that lets user compose a post");
    NSString *photoVideoButton = NSLocalizedString(@"Photo or Video", @"Title for button opens up camera to take photo or Video");
    NSString *audioButton = NSLocalizedString(@"Audio", @"Title for button that opens microphone to capture audio");
    
    // Shows the user options for selecting an image to post
    UIAlertController *assetMethod = [UIAlertController alertControllerWithTitle:alertTitle message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    assetMethod.modalPresentationStyle = UIModalPresentationPopover;
    UIPopoverPresentationController *popPresenter = assetMethod.popoverPresentationController;
    popPresenter.barButtonItem = self.navigationItem.rightBarButtonItem;
    
    // Create WeakSelf Variable
    __weak StreetExplorerViewController *weakSelf = self;
    
    // Initialize ImagePicker
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = weakSelf;
    imagePicker.allowsEditing = YES;
    // Create Action Variable
    UIAlertAction *photoVideoAsset, *audioAsset;
    
    // Verify which camera source are available
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        
        photoVideoAsset = [UIAlertAction actionWithTitle:photoVideoButton style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            
            NSArray *mediaTypesArray = [UIImagePickerController availableMediaTypesForSourceType:imagePicker.sourceType];
            imagePicker.mediaTypes = mediaTypesArray;
            
            [weakSelf presentViewController:imagePicker animated:YES completion:nil];
        }];
        
        [assetMethod addAction:photoVideoAsset];
    }
    
    audioAsset = [UIAlertAction actionWithTitle:audioButton style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [weakSelf performSegueWithIdentifier:@"smartStreetVCToAudioRecorderVCSegue" sender: weakSelf];
    }];
    
    [assetMethod addAction:audioAsset];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [assetMethod addAction:cancel];
    
    [self presentViewController:assetMethod animated:YES completion:nil];

}

- (IBAction)loginPressed:(UIButton *)sender {
    
    NSString * storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    UIViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    [self presentViewController:vc animated:YES completion:nil];
}

#pragma mark - Instance Methods
- (void)animateSearchBarTo:(BarVisualState)state {
    
    // Variable to hold new value passed to method; Set to visible initially
    float newAlphaValue = kSearchBarVisible;
    
    // Check if Hide was passed and change newAlpha value accordingly
    if(state == kHide) newAlphaValue = kSearchBarNotVisible;
    
    // Animation sequence to show a hidden search bar.
    [UIView animateWithDuration:kDefaultAnimationTime
                          delay:kDefaultDelayTime
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         
                         // Change search bar alpha to hide it
                         self.searchBar.alpha = newAlphaValue;
                         
                         // Called on parent view to ensure it is displayed correctly
                         [self.view layoutIfNeeded];
                         
                     } completion:nil];
}

- (void)animateNavBarTo:(BarVisualState)state {
    
    [self animateSearchBarTo:kHide];
    
    // Variable to hold new value passed to method; Set to visible initially
    float newHeightValue = kDefaultNavBarVerticalSpaceConst;
    
    // Check if Hide was passed and change newAlpha value accordingly
    if(state == kHide) newHeightValue = -kNavBarHeight;
    
    // Animation sequence to show a hidden nav bar.
    [UIView animateWithDuration:kDefaultAnimationTime
                          delay:kDefaultDelayTime
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         
                         // Change NavBar Vertical Space constant Height
                         self.navBarVerticalSpaceConst.constant = newHeightValue;
                         
                         // Called on parent view to ensure it is displayed correctly
                         [self.view layoutIfNeeded];
                         
                     } completion:nil];
}

- (void)hideActionViewsExceptViewForActionWithTag:(ActionButtonTag)actionTag {
    
    // Hide all views
    self.qrPreviewView.hidden = YES;
    self.mapView.hidden = YES;
    self.webView.hidden = YES;
    
    // Show Exception
    switch (actionTag) {
        case kQRCodeButtonTag:
            self.qrPreviewView.hidden = NO;
            [self animateNavBarTo:kHide];
            break;
        case kMapButtonTag:
            self.mapView.hidden = NO;
            break;
        case kPictureButtonTag:
            self.qrPreviewView.hidden = NO;
            break;
    }
}

- (void)startActionWithTag:(ActionButtonTag)actionTag {
    
    // Start Action
    switch (actionTag) {
        case kQRCodeButtonTag:
            [self startReadingQRCodes];
            break;
        case kMapButtonTag:
            // Start Action
            break;
        case kPictureButtonTag:
            // Start Action
            break;
    }
}

- (BOOL)startReadingQRCodes {
    
    // Initialize Capture Device (Video) and check for errors
    NSError *error;
    AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:&error];
    if (!input) {
        NSLog(@"%@", [error localizedDescription]);
        return NO;
    }
    
    // Initialize Capture Session and pass the Input Device (Video)
    self.captureSession = [[AVCaptureSession alloc] init];
    [self.captureSession addInput:input];
    AVCaptureMetadataOutput *captureMetadataOutput = [[AVCaptureMetadataOutput alloc] init];
    [self.captureSession addOutput:captureMetadataOutput];
    
    // Create a Queue to capture output on a different Thread and Specify QR Codes
    dispatch_queue_t dispatchQueue;
    dispatchQueue = dispatch_queue_create(kCaptureSessionQueue, NULL);
    [captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatchQueue];
    [captureMetadataOutput setMetadataObjectTypes:[NSArray arrayWithObject:AVMetadataObjectTypeQRCode]];
    
    // Set our preview View
    self.videoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.captureSession];
    [self.videoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [self.videoPreviewLayer setFrame:self.qrPreviewView.layer.bounds];
    [self.qrPreviewView.layer addSublayer:self.videoPreviewLayer];
    
    // Start Capturing Session
    [self.captureSession startRunning];
    
    return YES;
}

- (void)showNavBar:(NSTimer *)timer {
    // Nil Timer to avoid firing twice
    self.navBarHideTimer = nil;
    
    // Animate the movement of the Nav Bar back into place after the used moved the map
    [self animateNavBarTo:kShow];
}

- (NSURL *)checkIfDataIsURL:(NSString *)possibleURLString {
    
    NSURL *possibleURL = [NSURL URLWithString:possibleURLString];
    if (possibleURL && possibleURL.scheme && possibleURL.host){
        return possibleURL;
    } else {
        return nil;
    }
    
}

- (void)stopReading {
    [self.captureSession stopRunning];
    self.captureSession = nil;
    
    [self.videoPreviewLayer removeFromSuperlayer];
    
    [self animateNavBarTo:kShow];
}

- (void)loadBeepSound {
    NSString *beepFilePath = [[NSBundle mainBundle] pathForResource:@"beep" ofType:@"mp3"];
    NSURL *beepURL = [NSURL URLWithString:beepFilePath];
    NSError *error;
    
    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:beepURL error:&error];
    if (error) {
        NSLog(@"Could not play beep file.");
        NSLog(@"%@", [error localizedDescription]);
    }
    else{
        [self.audioPlayer prepareToPlay];
    }
}

- (void)showAlert {
    
    // Instantiate Help object
    HelpMethods *help = [[HelpMethods alloc]init];
    
    // Call Help method to show Alert Error Message to user
    [help showAlertOnViewController:self withTitle:kAlertTitle andMessage:kAlertMessage andActionTitle:kActionTitle];
}

- (void)loadWebPageWithUrl:(NSURL *)checkedURL {
    
    // Load a passed URL to our WebView and load page
    NSMutableURLRequest * request =[NSMutableURLRequest requestWithURL:checkedURL];
    [self.webView loadRequest:request];
}

- (void)startSearch:(NSString *)searchString {
    
    //  Cancel if already searching
    if (self.localSearch.searching)
        [self.localSearch cancel];
    
    // Confine the map search area to the user's current location.
    MKCoordinateRegion newRegion;
    newRegion.center.latitude = self.userCoordinate.latitude;
    newRegion.center.longitude = self.userCoordinate.longitude;
    
    //  Set Range: Small number = More Zoom
    newRegion.span.latitudeDelta = 0.050000;
    newRegion.span.longitudeDelta = 0.050000;
    
    // Initialize Search Request with region and using Natural language
    MKLocalSearchRequest *request = [[MKLocalSearchRequest alloc] init];
    request.naturalLanguageQuery = searchString;
    request.region = newRegion;
    
    // Creat Handle for asynchronous search
    MKLocalSearchCompletionHandler completionHandler = ^(MKLocalSearchResponse *response, NSError *error) {
        
        // Check for errors
        if (error != nil) {
            [self showAlert];  // Show Error Alert
        } else {
            // Get Map Items from response
            self.places = [response mapItems];
            
            // Set Property to store the Region from the response
            self.boundingRegion = response.boundingRegion;
            
            // Load the map with the results
            [self loadMapWithSearchResults];
        }
        // Hide the Network Activity Indicator
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    };
    
    // Set to nil to prepare for next Search
    if (self.localSearch != nil) {
        self.localSearch = nil;
    }
    
    // Intialize Search Object
    self.localSearch = [[MKLocalSearch alloc] initWithRequest:request];
    
    // Start Search asynchronous search
    [self.localSearch startWithCompletionHandler:completionHandler];
    
    // Show Network Indicator to user
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)loadMapWithSearchResults {
    
    // Adjust the map to zoom/center to the annotations we want to show.
    [self.mapView setRegion:self.boundingRegion animated:YES];
    
    // Set annotations in Map
    for (MKMapItem *item in self.places) {
        MapViewAnnotation *annotation = [[MapViewAnnotation alloc] init];
        annotation.coordinate = item.placemark.location.coordinate;
        annotation.title = item.name;
        
        // Verify URL and PHonenumber are available
        NSString *url = @"";
        NSString *phoneNumber = @"";
        
        if(item.url) url = [@", " stringByAppendingString:[item.url absoluteString]];
        if(item.phoneNumber) phoneNumber = item.phoneNumber;
        
        // Pass URL and Phonenumber info
        annotation.subtitle = [phoneNumber stringByAppendingString:url];
        annotation.phoneNumber = item.phoneNumber;
        annotation.item = item;
        [self.mapView addAnnotation:annotation];
    }
}

- (void)getDirectionsToDestination:(MKMapItem *)destination {
    
    // Intialize Reques and set Source, Destination and other properties
    MKDirectionsRequest *request = [[MKDirectionsRequest alloc] init];
    request.source = [MKMapItem mapItemForCurrentLocation];
    request.destination = destination;
    request.requestsAlternateRoutes = NO;
    
    // Intialize Directions Calculations Asynchronous
    MKDirections *directions = [[MKDirections alloc] initWithRequest:request];
    [directions calculateDirectionsWithCompletionHandler:
     ^(MKDirectionsResponse *response, NSError *error) {
         if (error) {
             NSLog(@"An Error Ocurred Getting directions %@", error);
         } else {
             // If No Error Show routes in Map
             [self showRoute:response];
         }
     }];
}

- (void)showRoute:(MKDirectionsResponse *)response {
    
    // Remove Old Overlays
    [self.mapView removeOverlays:self.mapView.overlays];
    
    // Loop through the routes
    for (MKRoute *route in response.routes)
    {
        // Draw Route
        [self.mapView addOverlay:route.polyline level:MKOverlayLevelAboveRoads];
        
        // Send Route steps to Reveal Controller to present in tableView
        if(!self.directionsTableViewController){
            SWRevealViewController *revealViewC = (SWRevealViewController *)self.parentViewController;
            self.directionsTableViewController = (DirectionsTableViewController *)[revealViewC.rightViewController.childViewControllers objectAtIndex:0];
            
            // Set Controller as Delegate to ask for Direction when user activates view
            [self.directionsTableViewController setDelegate:self];
        }
        // Store Route as a Property
        self.currentRoute = route;
    }
}

- (void)removeCurrentAnnotations {
    
    NSMutableArray *annotations = [NSMutableArray arrayWithArray:self.mapView.annotations];
    [annotations removeObject:self.pin];
    
    [self.mapView removeAnnotations:annotations];
}

#pragma mark - Map View Delegate
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    
    // If it's the user location, just return nil.
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    // Handle any custom annotations.
    if ([annotation isKindOfClass:[MapViewAnnotation class]])
    {
        MapViewAnnotation *mapViewAnnotation = (MapViewAnnotation *)annotation;
        
        // Try to dequeue an existing pin view first.
        MKAnnotationView *pinView = (MKAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"CustomPinAnnotationView"];
        
        // Initialize Helper
        HelpMethods *help = [[HelpMethods alloc]init];
        
        UIImage *customImage;
        switch ([help identifySearchText:self.searchBar.text]) {
            case 0:
                customImage = [UIImage imageNamed:@"default.png"];
                break;
            case 1:
                customImage = [UIImage imageNamed:@"restaurant.png"];
                break;
            case 2:
                customImage = [UIImage imageNamed:@"movie.png"];
                break;
            case 3:
                customImage = [UIImage imageNamed:@"hospital.png"];
                break;
            case 4:
                customImage = [UIImage imageNamed:@"hotel.png"];
                break;
            case 5:
                customImage = [UIImage imageNamed:@"airport.png"];
                break;
            case 6:
                customImage = [UIImage imageNamed:@"mall.png"];
                break;
        }
        
        if (!pinView)
        {
            // If an existing pin view was not available, create one.
            pinView = [[MKAnnotationView alloc] initWithAnnotation:mapViewAnnotation reuseIdentifier:@"CustomPinAnnotationView"];
            pinView.canShowCallout = YES;
            pinView.image = customImage;
            pinView.calloutOffset = CGPointMake(0, -20);
            
            // Add a detail disclosure button to the callout.
            UIButton* rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [rightButton setFrame:CGRectMake(0,0,22,22)];
            [rightButton setImage:[UIImage imageNamed:@"direction-icon"] forState:UIControlStateNormal];
            
            pinView.rightCalloutAccessoryView = rightButton;
            MapViewAnnotation *annot = (MapViewAnnotation *)pinView.annotation;
            
            // Add an image to the left callout.
            if(annot.image){
                UIImageView *iconView = [[UIImageView alloc] initWithImage:annot.image];
                pinView.leftCalloutAccessoryView = iconView;
            } else {
                pinView.leftCalloutAccessoryView = nil;
            }
            
        } else {
            pinView.annotation = (MapViewAnnotation *)annotation;
            pinView.image = customImage;
            
            MapViewAnnotation *annot = (MapViewAnnotation *)pinView.annotation;
            // Add an image to the left callout.
            if(annot.image){
                UIImageView *iconView = [[UIImageView alloc] initWithImage:annot.image];
                pinView.leftCalloutAccessoryView = iconView;
            } else {
                pinView.leftCalloutAccessoryView = nil;
            }
            
        }
        return pinView;
    }
    
    return nil;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    NSLog(@"didSelectAnnotationView");
    
    // get annotation and verify Kind;
    id <MKAnnotation> annotation = [view annotation];
    if ([annotation isKindOfClass:[MapViewAnnotation class]]){
        
        // then cast to MapViewAnnotation Class
        MapViewAnnotation *mapViewAnnotation = (MapViewAnnotation *)annotation;
        
        // Loop thru records and compare record id with selected annotation
        for (MKMapItem *currentItem in self.places){
            // If match found, calculate IndexPath and select programmatically select table row.
            if(currentItem == mapViewAnnotation.item){
                self.selectedItem = currentItem;
            }
        }
    }
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    // here we illustrate how to detect which annotation type was clicked on for its callout
    id <MKAnnotation> annotation = [view annotation];
    if ([annotation isKindOfClass:[MapViewAnnotation class]])
    {
        [self getDirectionsToDestination:self.selectedItem];
    }
}

- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated {
    if(!(firstMapMove)){
        
        // Invalidate timer if it has not fired yet, since the user moved the map again.
        [self.navBarHideTimer invalidate];
        
        // Animate the Hiding of the NavBar when the user move the Map.
        [self animateNavBarTo:kHide];
        
    }
    
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    if(!(firstMapMove)){
        self.navBarHideTimer = [NSTimer scheduledTimerWithTimeInterval:kNavBarHideTime
                                                                target:self
                                                              selector:@selector(showNavBar:)
                                                              userInfo:nil
                                                               repeats:NO];
    }
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id < MKOverlay >)overlay {
    
    // Render Route as Overlay
    MKPolylineRenderer *renderer =
    [[MKPolylineRenderer alloc] initWithOverlay:overlay];
    renderer.strokeColor = [UIColor blueColor];
    renderer.lineWidth = 5.0;
    
    return renderer;
}

#pragma mark - CLLocationManagerDelegate Methods
- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {

    // User Authorized app to use Location Services
    if(status != kCLAuthorizationStatusNotDetermined)
        [self.locationManager startUpdatingLocation];
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    self.currentLocation = locations.lastObject;
    self.userCoordinate = self.currentLocation.coordinate;
    
    // Set User Location in Map
    if (!self.pin) {
        self.pin = [[MKPointAnnotation alloc]init];
        self.pin.coordinate = self.currentLocation.coordinate;
        
        [self.mapView addAnnotation:self.pin];
        [self.mapView showAnnotations:@[self.pin] animated:NO];
        
        [self.locationManager stopUpdatingLocation];
        
        // Flag the initial location update
        firstMapMove = NO;
    } else {
        self.pin.coordinate = self.currentLocation.coordinate;
        
        [self.mapView addAnnotation:self.pin];
        [self.mapView showAnnotations:@[self.pin] animated:NO];
        
        [self.locationManager stopUpdatingLocation];
    }
    
    // Stop Spinner
    [self.userSpinner stopAnimating];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"Error Locating user Location Manager: %@", error);
    if([error.domain isEqualToString: kCLErrorDomain])
        NSLog(@"Error Updating Location");
    else
        NSLog(@"Other Location Error");
    
    firstMapMove = NO;
}

#pragma mark - UIWebViewDelegate Methods
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    // Start Spinner
    [self.webViewSpinner startAnimating];
    
    // Clear Previous Message
    [(UITextView *)[self.qrPreviewView.subviews lastObject] setText:@""];
    
    // Hide QR Preview Window
    self.qrPreviewView.hidden = YES;
    
    // Show Web VIew
    self.webView.hidden = NO;
    
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    // Stop Spinner
    [self.webViewSpinner stopAnimating];

}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    // Stop Spinner
    [self.webViewSpinner stopAnimating];
    
    self.qrPreviewView.hidden = NO;
    self.webView.hidden = YES;
    
    [(UITextView *)[self.qrPreviewView.subviews lastObject] setText:@"Error Loading the QR Code URL. This could be due to a Server Problem. Please, Try Again Later"];
    
}

#pragma mark - DirectionsTableViewControllerDelegate
-(MKRoute *)getRouteInformation {
    return self.currentRoute;
}

#pragma mark - UISearchBarDelegate
- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar {
    [searchBar resignFirstResponder];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:YES animated:YES];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:NO animated:YES];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    // If the text changed, reset the tableview if it wasn't empty.
    if (self.places.count != 0) {
        
        // Set the list of places to be empty.
        self.places = @[];
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    
    // Check if location services are available
    if ([CLLocationManager locationServicesEnabled] == NO) {
        NSLog(@"%s: location services are not available.", __PRETTY_FUNCTION__);
        
        // Display alert to the user.
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Location services"
                                                                       message:@"Location services are not enabled on this device. Please enable location services in settings."
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}]; // Do nothing action to dismiss the alert.
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        
        return;
    }
    
    // Request "when in use" location service authorization.
    // If authorization has been denied previously, we can display an alert if the user has denied location services previously.
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined) {
        [self.locationManager requestWhenInUseAuthorization];
    } else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied) {
        NSLog(@"%s: location services authorization was previously denied by the user.", __PRETTY_FUNCTION__);
        
        // Display alert to the user.
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Location services"
                                                                       message:@"Location services were previously denied by the user. Please enable location services for this app in settings."
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}]; // Do nothing action to dismiss the alert.
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        
        return;
    }
    
    // Start Search Immediatelly when user press button; we already have a location.
    [self startSearch:self.searchBar.text];
    
    // Remove Previous Overlays
    [self.mapView removeOverlays:self.mapView.overlays];
    
    // Remove Previous Annotations
    [self removeCurrentAnnotations];
    
}

#pragma mark UIImagePickerControllerDelegate
-(void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    NSString *type = [info objectForKey:UIImagePickerControllerMediaType];
    
    if ([type isEqualToString:(NSString *)kUTTypeVideo] || [type isEqualToString:(NSString *)kUTTypeMovie])
    {
        NSURL *videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
        NSString *moviePath = [videoURL path];
        if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum (moviePath))
        {
            UISaveVideoAtPathToSavedPhotosAlbum (moviePath,self, @selector(video:didFinishSavingWithError:contextInfo:), nil);
        }

        self.currentMediaURL = [NSURL fileURLWithPath:[self stringPathWithURL:videoURL orData:nil andFileName:@"/tempVideo.mov"]];
    }
    if ([type isEqualToString:(NSString *)kUTTypeImage]) {
        
        NSData *jpgData = UIImageJPEGRepresentation(info[UIImagePickerControllerOriginalImage], 1.0);
        UIImageWriteToSavedPhotosAlbum(info[UIImagePickerControllerOriginalImage], self, @selector(video:didFinishSavingWithError:contextInfo:), nil);
        
        self.currentMediaURL = [NSURL fileURLWithPath:[self stringPathWithURL:nil orData:jpgData andFileName:@"/tempPhoto.jpg"]];
    }
    
    // Dismisses imagePicker Add image to Button
    [picker dismissViewControllerAnimated:YES completion:^{
        [self performSegueWithIdentifier:@"smartStreetVCToShareVCSegue" sender:self];
    }];
}

#pragma mark UIImagePicker Convenience Methods
-(NSString *)stringPathWithURL:(NSURL *)mediaURL orData:(NSData *)data andFileName:(NSString *)fileName {
    
    if(!data){
        data = [NSData dataWithContentsOfURL:mediaURL];
    }
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *tempPath = [documentsDirectory stringByAppendingFormat:@"%@", fileName];
    if([data writeToFile:tempPath atomically:NO]) {
        return tempPath;
    } else {
        // TO DO: create Alert to User to let them know of failure
        return nil;
    }
}

- (void)video:(NSString*)videoPath didFinishSavingWithError:(NSError*)error contextInfo:(void*)contextInfo {
    if (error)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Photo/Video Saving Failed"  delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil, nil];
        [alert show];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Photo/Video Saved" message:@"Saved To Photo Album"  delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
}

#pragma mark -  AVCaptureMetadataOutputObjectDelegate Methods
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection {
    
    if (metadataObjects != nil && [metadataObjects count] > 0) {
        AVMetadataMachineReadableCodeObject *metadataObj = [metadataObjects objectAtIndex:0];
        if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeQRCode]) {
            
            NSURL *urlFromMetadata = [self checkIfDataIsURL:[metadataObj stringValue]];
            if(urlFromMetadata) {
                [self performSelectorOnMainThread:@selector(stopReading) withObject:nil waitUntilDone:NO];
                [self performSelectorOnMainThread:@selector(loadWebPageWithUrl:) withObject:urlFromMetadata waitUntilDone:NO];
            } else {
                
                [self performSelectorOnMainThread:@selector(stopReading) withObject:nil waitUntilDone:NO];
                [(UITextView *)[self.qrPreviewView.subviews lastObject] performSelectorOnMainThread:@selector(setText:) withObject:[metadataObj stringValue] waitUntilDone:NO];
            }
            
            if (self.audioPlayer) {
                [self.audioPlayer play];
            }
            
        }
    }
}

#pragma mark - Navigation Methods Segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    // Segue to Edit Shoe for Container View
    if ([segue.identifier isEqualToString: @"smartStreetVCToShareVCSegue"]) {
        
        UINavigationController *navigationController = (UINavigationController *)[segue destinationViewController];
        ShareViewController *shareViewController = (ShareViewController *)[navigationController.childViewControllers firstObject];
        shareViewController.mediaURL = self.currentMediaURL;
        
    }
}

@end