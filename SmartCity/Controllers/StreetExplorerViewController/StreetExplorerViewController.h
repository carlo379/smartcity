//
//  StreetExplorerViewController.h
//  SmartCity
//
//  Created by Carlos Martinez on 2/25/16.
//  Copyright © 2016 Carlos Martinez. All rights reserved.
//
@import MapKit;

#pragma mark - Interface
@interface StreetExplorerViewController : UIViewController

#pragma mark - Public Properties
@property (weak, nonatomic) IBOutlet UIButton *leftSideMenu;
@property (weak, nonatomic) IBOutlet UIButton *rightSideMenu;
@property (strong, nonatomic) UIRefreshControl *refreshControl;

@end

