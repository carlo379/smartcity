//
//  InteractViewController.m
//  SmartCity
//
//  Created by Carlos Martinez on 4/23/16.
//  Copyright © 2016 Carlos Martinez. All rights reserved.
//

#import "InteractViewController.h"
#import "Firebase.h"
#import <AVFoundation/AVFoundation.h>
#import <CZPicker.h>
#import "HelpMethods.h"
#import <CoreLocation/CoreLocation.h>
#import "Constants.h"

@interface InteractViewController ()<AVAudioPlayerDelegate, CZPickerViewDataSource, CZPickerViewDelegate, CLLocationManagerDelegate>

// FIREBASE DB
@property (strong, nonatomic) Firebase *dbRef;

// LIGHT
@property (weak, nonatomic) IBOutlet UISwitch *lightSwitch;
@property (weak, nonatomic) IBOutlet UIView *lightView;
@property (weak, nonatomic) IBOutlet UIImageView *bulbView;
@property (weak, nonatomic) IBOutlet UISlider *intensitySlider;
@property (weak, nonatomic) IBOutlet UISlider *colorSlider;
@property (strong, nonatomic) Firebase *lightIteractionsRef;
@property (strong, nonatomic) Firebase *lightInteractionsRefAutoID;
@property (strong, nonatomic) NSMutableArray *lightInteractionArray;
@property (nonatomic) CGFloat hue;
@property (nonatomic) CGFloat sat;

// MUSIC
@property (weak, nonatomic) IBOutlet UISwitch *musicSwitch;
@property (strong, nonatomic) AVAudioPlayer *audioPlayer;
@property (weak, nonatomic) IBOutlet UILabel *musicTitleLabel;
@property NSArray *music;
@property NSString *selectedMusic;
@property (strong, nonatomic) Firebase *musicIteractionsRef;
@property (strong, nonatomic) Firebase *musicInteractionsRefAutoID;
@property (strong, nonatomic) NSMutableArray *musicInteractionArray;

// MUSIC PICKER
@property CZPickerView *pickerWithImage;

// Data From Firebase
@property (strong, nonatomic) NSMutableDictionary *treeURLs;
@property (strong, nonatomic) NSMutableArray *treeArray;
@property (strong, nonatomic) NSArray *treeSensorsArray;
@property (strong, nonatomic) NSMutableDictionary *treeDataDict;
@property (strong, nonatomic) NSMutableArray *treeSensorDataArray;

// Help Property
@property (strong, nonatomic) HelpMethods *help;

// Location Property
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) CLLocation *currentLocation;
@property (nonatomic) CLLocationCoordinate2D userCoordinate;
@property (strong, nonatomic) CLLocation *treeLocation;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UIView *outOfRangeView;
@property (weak, nonatomic) IBOutlet UILabel *navBarDistanceLabel;
@property (nonatomic) BOOL outOfRangeFlag;
@end

@implementation InteractViewController
#pragma mark - Life Cycle View Methods
- (void)viewDidLoad {
    [super viewDidLoad];

    // Signal controller that need to update Status Bar for change in color
    [self setNeedsStatusBarAppearanceUpdate];
    
    self.music = @[@"Clearday", @"Littleidea", @"Energy"];
    self.selectedMusic = self.music[0];
    self.musicTitleLabel.text = self.selectedMusic;

    /***** SETUP LIGHT *****/
    // Init an Array to store Firebase Data
    self.lightInteractionArray = [[NSMutableArray alloc]init];
    
    // Base URL for Firebase
    self.dbRef = [[Firebase alloc] initWithUrl:@"https://smarttree.firebaseio.com"];
    
    /***** SETUP MUSIC *****/
    // Init an Array to store Firebase Data
    self.musicInteractionArray = [[NSMutableArray alloc]init];
    
    // Initialize data properties from Firebase: Tree Data
    self.treeURLs = [[NSMutableDictionary alloc]init];
    self.treeArray = [[NSMutableArray alloc]init];
    self.treeSensorsArray = [[NSArray alloc]init];
    self.treeDataDict = [[NSMutableDictionary alloc]init];
    self.treeSensorDataArray = [[NSMutableArray alloc]init];

    // Instantiate Helper object
    self.help = [[HelpMethods alloc]init];
    
    // Listen for changes to data in Firebase Backend
    [self listenToFirebaseDbForTreeEvents];
    
    // Disable Switches
    self.lightSwitch.enabled = NO;
    self.musicSwitch.enabled = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.locationManager stopUpdatingLocation];
    
    UISwitch *dummySwitch = [[UISwitch alloc]init];
    [dummySwitch setOn:false];
    [self musicSwitchPressed:dummySwitch];
    [self lightSwitchPressed:dummySwitch];
    
    [self.musicSwitch setOn:NO animated:YES];
    [self.lightSwitch setOn:NO animated:YES];
}

#pragma mark - UIViewController Methods Override
- (UIStatusBarStyle)preferredStatusBarStyle {
    // Update color/style of StatusBar
    return UIStatusBarStyleLightContent;
}

#pragma mark - Custom Methods
- (void)listenToFirebaseDbForTreeEvents {
    
    // Get Reference to the Firebase Database
    Firebase *refLight = [[Firebase alloc] initWithUrl: @"https://smarttree.firebaseio.com/tree"];
    
    // Attach a block to read the data at our posts reference
    [refLight observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
        
        if(snapshot.value == [NSNull null]) {
            NSLog(@"No messages");
        } else {
            for (FDataSnapshot* childSnap in snapshot.children) {
                [self.treeArray addObject:childSnap.key];
                self.treeDataDict = childSnap.value;
                
                
                NSDictionary *dict = childSnap.value;
                NSLog(@"Dict %@", dict);
                
                NSLog(@"Dict elements %@", dict.allKeys);
                
                if([dict.allKeys containsObject:@"sensor"]){
                    NSDictionary *sensorsDict = dict[@"sensor"];
                    NSLog(@"Dict elements %@", sensorsDict.allKeys);
                    self.treeSensorsArray = sensorsDict.allKeys;
                    for(NSDictionary *sensor in sensorsDict){
                        [self.treeSensorDataArray addObject:sensor];
                    }
                    // Got: treeArray           - All Tree IDs
                    //      treeSensorDataArray - Dict with data from Tree
                    // Got: treeSensorArray     - All Sensor IDs
                    //      treeSensorDataArray - Array with data from sensors
                }
                // Prepare Dashboard
                [self prepareForInteraction];
            }
        }
        NSLog(@"FInish Parsing");
    } withCancelBlock:^(NSError *error) {
        NSLog(@"%@", error.description);
    }];
}

- (void)prepareForInteraction {
    
    //Initialize Location Manager
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    self.locationManager.delegate = self;
    
    // Prepare URLs for connections
    // Create a reference to a Firebase database URL
    if(self.treeArray.count>0){
        NSString *treePortion = [NSString stringWithFormat:@"tree/%@",self.treeArray[0]];
        for(int i=0; i<self.treeSensorsArray.count; i++){
            NSString *sensorID = self.treeSensorsArray[i];
            
            NSString *sensorPortion;
            NSRange range = [sensorID rangeOfString:@"^SEB[0-9]" options:NSRegularExpressionSearch];
            if(range.location != NSNotFound){
                sensorPortion = [NSString stringWithFormat:@"/sensor/%@/lightInteractions", sensorID];
                NSString *fullPath = [treePortion stringByAppendingString:sensorPortion];
                self.lightIteractionsRef = [self.dbRef childByAppendingPath: fullPath];
            } else {
                sensorPortion = [NSString stringWithFormat:@"/sensor/%@/musicInteractions", sensorID];
                NSString *fullPath = [treePortion stringByAppendingString:sensorPortion];
                self.musicIteractionsRef = [self.dbRef childByAppendingPath: fullPath];
            }
        }
    }
}

#pragma mark - CLLocationManagerDelegate Methods
- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    
    // User Authorized app to use Location Services
    if(status != kCLAuthorizationStatusNotDetermined)
        [self.locationManager startUpdatingLocation];
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    // Set Propertity to current location
    self.currentLocation = locations.lastObject;
    self.userCoordinate = self.currentLocation.coordinate;
    
    
    for(NSString *tree in self.treeArray){
        NSString *pathURL = [@"tree/" stringByAppendingString:tree];
        self.treeURLs[tree] = pathURL;
        
        for(int i=0; i<self.treeSensorsArray.count;i++){
            
            // Tree Data Start
            NSDictionary *maintenance = self.treeDataDict[@"maintenance"];
            NSDictionary *autoID = maintenance[[maintenance.allKeys objectAtIndex:0]];
            NSNumber *latitude = autoID[@"latitude"];
            NSNumber *longitude = autoID[@"longitude"];
            self.treeLocation = [[CLLocation alloc] initWithLatitude:[latitude doubleValue] longitude:[longitude doubleValue]];
            // Tree Data End
            
        }
        
        int distance = [locations.lastObject distanceFromLocation:self.treeLocation];
        self.distanceLabel.text = [NSString stringWithFormat:@"Distance: %d", distance];
        self.navBarDistanceLabel.text = self.distanceLabel.text;
        
        if(distance <= 10){
            // Set Flag to signal out of range and avoid recording changes
            self.outOfRangeFlag = false;
            
            // Animation sequence to hide Out oF Range View.
            [self outOfRangeViewAlphaTo:0.0];
            
            for(int i=0; i<self.treeSensorsArray.count;i++){
                
                // Sensor Data Start
                NSDictionary *sensorsDict = self.treeDataDict[@"sensor"];
                NSArray *sensorKeyArray = sensorsDict.allKeys;
                for(NSString *key in sensorKeyArray) {
                    NSDictionary *maintenance = sensorsDict[key];
                    NSDictionary *maintData = maintenance[@"maintenance"];
                    NSDictionary *autoID = maintData[[maintData.allKeys objectAtIndex:0]];
                    
                    NSString *type = autoID[@"type"];
                    if([type isEqualToString:@"speaker"]){
                        self.musicSwitch.enabled = YES;
                    }
                    if([type isEqualToString:@"bulb"]){
                        self.lightSwitch.enabled = YES;
                    }
                }
                // Sensor Data End
            }
        } else {
            
            // Set Flag to signal out of range and avoid recording changes
            self.outOfRangeFlag = true;
            
            // Animation sequence to show Out oF Range View.
            [self outOfRangeViewAlphaTo:0.95];
            
            // Turn off light switch
            UISwitch *dummySwitch = [[UISwitch alloc]init];
            [dummySwitch setOn:false];
            [self lightSwitchPressed:dummySwitch];
            [self.lightSwitch setOn:NO animated:YES];
            self.lightSwitch.enabled = NO;
            
            //Turn off musicSwitch
            [self musicSwitchPressed:dummySwitch];
            [self.musicSwitch setOn:NO animated:YES];

            self.musicSwitch.enabled = NO;

        }
    }


    
    
}

- (void)outOfRangeViewAlphaTo:(float )alphaVal {

    // Animation sequence to show a hidden nav bar.
    [UIView animateWithDuration:kDefaultAnimationTime
                          delay:kDefaultDelayTime
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         
                         // Change NavBar Vertical Space constant Height
                         self.outOfRangeView.alpha = alphaVal;
                         
                         // Called on parent view to ensure it is displayed correctly
                         [self.view layoutIfNeeded];
                         
                     } completion:^(BOOL finished) {
                         if(alphaVal == 0.0) {
                             self.outOfRangeView.hidden = YES;
                         } else {
                             self.outOfRangeView.hidden = NO;
                         }
                     }];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"Error Locating user Location Manager: %@", error);
    if([error.domain isEqualToString: kCLErrorDomain])
        NSLog(@"Error Updating Location");
    else
        NSLog(@"Other Location Error");
}

#pragma mark - IBActions Methods
- (IBAction)lightSwitchPressed:(UISwitch *)sender {
    if(sender.on){
        self.hue = (CGFloat)(self.colorSlider.value/359);
        self.sat = (CGFloat)(self.intensitySlider.value/100);
        
        [UIView animateWithDuration:0.5 animations:^{
            
            self.lightView.backgroundColor =[UIColor colorWithHue:self.hue
                                                       saturation:self.sat
                                                       brightness:0.96
                                                            alpha:1.0];
            
            
        } completion:^(BOOL finished) {
            NSLog(@"Switch On-------");
            
            // Record Only if NOT out of range
            if(!self.outOfRangeFlag) {
                // Record Changges to Firebase Database
                [self recordChangesToLight];
            }
        }];
        
    } else {
        
        self.hue = (CGFloat)(220/359);
        self.sat = (CGFloat)(6/100);
        
        [UIView animateWithDuration:0.2 animations:^{
            self.lightView.backgroundColor = [UIColor colorWithHue:self.hue
                                                        saturation:self.sat
                                                        brightness:0.96
                                                             alpha:1.0];
        } completion:^(BOOL finished) {
            NSLog(@"Switch Off-------");
            
            // Record Only if NOT out of range
            if(!self.outOfRangeFlag) {
                // Record Changges to Firebase Database
                [self recordChangesToLight];
            }
        }];
    }
    
}

- (IBAction)intensityChanged:(UISlider *)sender {
    
    if(self.lightSwitch.on){
        self.hue = (CGFloat)(self.colorSlider.value/359);
        self.sat = (CGFloat)(self.intensitySlider.value/100);
        
        [UIView animateWithDuration:0.5 animations:^{
            
            self.lightView.backgroundColor =[UIColor colorWithHue:self.hue
                                                       saturation:self.sat
                                                       brightness:0.96
                                                            alpha:1.0];
        } completion:^(BOOL finished) {
            NSLog(@"Intensity Changed-------");
        }];
    }
}

- (IBAction)intensityTouchUpInside:(UISlider *)sender {
    NSLog(@"Finish Changing Intensity");
    
    if(self.lightSwitch.on){
        // Record Changges to Firebase Database
        [self recordChangesToLight];
    }
}

- (IBAction)colorChanged:(UISlider *)sender {
    
    if(self.lightSwitch.on){
        self.hue = (CGFloat)(self.colorSlider.value/359);
        self.sat = (CGFloat)(self.intensitySlider.value/100);
        
        [UIView animateWithDuration:0.5 animations:^{
            
            self.lightView.backgroundColor =[UIColor colorWithHue:self.hue
                                                       saturation:self.sat
                                                       brightness:0.96
                                                            alpha:1.0];
        } completion:^(BOOL finished) {
            NSLog(@"Color Changed-------");
        }];
    }
}

- (IBAction)colorTouchUpInside:(UISlider *)sender {
    
    NSLog(@"Finish Changing Color");
    
    if(self.lightSwitch.on){
        // Record Changges to Firebase Database
        [self recordChangesToLight];
    }
}

- (IBAction)cancelPressed:(UIBarButtonItem *)sender {
    // Dismiss Comments View COntroller
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)musicSwitchPressed:(UISwitch *)sender {
    
    if(sender.on){
        
        [self prepareAndPlayMusic];
        
    } else {
        [self stopAudio:sender];
        
        // Record Only if NOT out of range
        if(!self.outOfRangeFlag) {
            // Record Changges to Firebase Database
            [self recordChangesToMusic];
        }
    }
}

- (IBAction)changeMusicPressed:(UIButton *)sender {
    
    if(self.musicSwitch.on){
        CZPickerView *picker = [[CZPickerView alloc] initWithHeaderTitle:@"Select Music" cancelButtonTitle:@"Cancel" confirmButtonTitle:@"Confirm"];
        picker.headerTitleFont = [UIFont systemFontOfSize: 40];
        picker.delegate = self;
        picker.dataSource = self;
        picker.needFooterView = NO;
        [picker show];
    }
}



#pragma mark - Sensor Interactions Methods
- (void)recordChangesToLight {
    
    // Write data to Firebase in Dictionary
    
    NSDictionary *lightInteractioDict = @{
                                          @"lightState" : [NSNumber numberWithBool:self.lightSwitch.on],
                                          @"hue" : [NSNumber numberWithFloat:self.hue],
                                          @"sat" : [NSNumber numberWithFloat:self.sat],
                                          };
    // Generate New Auto ID
    self.lightInteractionsRefAutoID = [self.lightIteractionsRef childByAutoId];
    
    // Post to Firebase Database
    [self.lightInteractionsRefAutoID setValue: lightInteractioDict];
}

- (void)recordChangesToMusic {
    
    // Write data to Firebase in Dictionary
    
    NSDictionary *musicInteractioDict = @{
                                          @"musicState" : [NSNumber numberWithBool:self.musicSwitch.on],
                                          @"musicSelection" : self.selectedMusic
                                          };
    // Generate New Auto ID
    self.musicInteractionsRefAutoID = [self.musicIteractionsRef childByAutoId];
    
    // Post to Firebase Database
    [self.musicInteractionsRefAutoID setValue: musicInteractioDict];
}

- (void)playAudio:(UISwitch *)sender {
    if(self.musicSwitch.on){
        [_audioPlayer play];
    }
}

- (void)stopAudio:(UISwitch *)sender {
    
    if(!sender.on){
        [_audioPlayer stop];
    }
}

#pragma mark - AVAudioPlayerDelegate Methods
- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
    NSLog(@"Success...Playing Musing");
}

- (void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error {
    NSLog(@"Error...Playing Musing");

}

- (void)audioPlayerBeginInterruption:(AVAudioPlayer *)player {
    NSLog(@"Interruption...Playing Musing");

}

- (void)audioPlayerEndInterruption:(AVAudioPlayer *)player {
    NSLog(@"End Interruption...Playing Musing");

}

- (NSString *)czpickerView:(CZPickerView *)pickerView titleForRow:(NSInteger)row{
    return self.music[row];
}

- (NSInteger)numberOfRowsInPickerView:(CZPickerView *)pickerView{
    return self.music.count;
}

- (void)czpickerView:(CZPickerView *)pickerView didConfirmWithItemAtRow:(NSInteger)row{
    NSLog(@"%@ is chosen!", self.music[row]);
    self.selectedMusic = self.music[row];
    self.musicTitleLabel.text = self.selectedMusic;
    [self prepareAndPlayMusic];

}

- (void)prepareAndPlayMusic {
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle]
                                         pathForResource:self.selectedMusic
                                         ofType:@"mp3"]];
    
    NSError *error;
    _audioPlayer = [[AVAudioPlayer alloc]
                    initWithContentsOfURL:url
                    error:&error];
    if (error)
    {
        NSLog(@"Error in audioPlayer: %@",
              [error localizedDescription]);
    } else {
        _audioPlayer.delegate = self;
        [_audioPlayer prepareToPlay];
        [self playAudio:nil];
        
        // Record Changges to Firebase Database
        [self recordChangesToMusic];
    }
}

@end
