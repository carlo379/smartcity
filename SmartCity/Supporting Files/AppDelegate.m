//
//  AppDelegate.m
//  SmartCity
//
//  Created by Carlos Martinez on 2/25/16.
//  Copyright © 2016 Carlos Martinez. All rights reserved.
//

#import "AppDelegate.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [super application:application didFinishLaunchingWithOptions:launchOptions];

    // We use the device orientation to set the video orientation of the video preview,
    // and to set the orientation of still images and recorded videos.
    
    // Inform the device that we want to use the device orientation.
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    
    // Facebook Delegate method
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:nil forKey:@"LoggedUser"];
    
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    [super application:application openURL:url sourceApplication:sourceApplication annotation:annotation];

    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                  openURL:url
                                                        sourceApplication:sourceApplication
                                                               annotation:annotation
                    ];
    // Add any custom logic here.
    return handled;
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Inform the device that we no longer require access the device orientation.
    [[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:nil forKey:@"LoggedUser"];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Inform the device that we want to use the device orientation again.
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Let the device power down the accelerometer if not used elsewhere while backgrounded.
    [[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [FBSDKAppEvents activateApp];
}

@end
