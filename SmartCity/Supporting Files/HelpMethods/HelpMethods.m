//
//  HelpMethods.m
//  SmartCity
//
//  Created by Carlos Martinez on 2/25/16.
//  Copyright © 2016 Carlos Martinez. All rights reserved.
//

#import "HelpMethods.h"

#pragma mark - Constants
static NSString * const kErrorMessage = @"Error Updating Context";

#pragma mark - Implementation
@implementation HelpMethods

#pragma mark - Instance Methods
-(void)showAlertOnViewController:(UIViewController *)viewController withTitle:(NSString *)title andMessage:(NSString *)message andActionTitle:(NSString *)actionTitle {
    
    // Create AlertController
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:title
                                          message:message
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    // Create Action for AlertController
    UIAlertAction *dissmisAction = [UIAlertAction actionWithTitle:actionTitle
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction *actionPerformed){
                                                              NSLog(kErrorMessage);
                                                          }];
    // Add Action to AlertController
    [alertController addAction:dissmisAction];
    
    // Present Alert Controller
    [viewController presentViewController:alertController animated:YES completion:nil];
}

- (int)identifySearchText:(NSString *)text {
    if ([text localizedCaseInsensitiveContainsString:@"restaurant"]) return 1;
    if ([text localizedCaseInsensitiveContainsString:@"food"]) return 1;
    if ([text localizedCaseInsensitiveContainsString:@"pizza"]) return 1;
    if ([text localizedCaseInsensitiveContainsString:@"eat"]) return 1;
    if ([text localizedCaseInsensitiveContainsString:@"movie"]) return 2;
    if ([text localizedCaseInsensitiveContainsString:@"teather"]) return 2;
    if ([text localizedCaseInsensitiveContainsString:@"entertainment"]) return 2;
    if ([text localizedCaseInsensitiveContainsString:@"hospital"]) return 3;
    if ([text localizedCaseInsensitiveContainsString:@"doctor"]) return 3;
    if ([text localizedCaseInsensitiveContainsString:@"pharmacy"]) return 3;
    if ([text localizedCaseInsensitiveContainsString:@"hotel"]) return 4;
    if ([text localizedCaseInsensitiveContainsString:@"house"]) return 4;
    if ([text localizedCaseInsensitiveContainsString:@"motel"]) return 4;
    if ([text localizedCaseInsensitiveContainsString:@"stay"]) return 4;
    if ([text localizedCaseInsensitiveContainsString:@"airport"]) return 5;
    if ([text localizedCaseInsensitiveContainsString:@"flight"]) return 5;
    if ([text localizedCaseInsensitiveContainsString:@"plane"]) return 5;
    if ([text localizedCaseInsensitiveContainsString:@"shop"]) return 6;
    if ([text localizedCaseInsensitiveContainsString:@"mall"]) return 6;
    if ([text localizedCaseInsensitiveContainsString:@"mart"]) return 6;
    if ([text localizedCaseInsensitiveContainsString:@"store"]) return 6;
    else return 0;
}

@end
