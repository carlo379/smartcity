//
//  AppDelegate.h
//  SmartCity
//
//  Created by Carlos Martinez on 2/25/16.
//  Copyright © 2016 Carlos Martinez. All rights reserved.
//

@import UIKit;
#import <FirebaseUI/FirebaseAppDelegate.h>

@interface AppDelegate : FirebaseAppDelegate <UIApplicationDelegate>

//@property (strong, nonatomic) UIWindow *window;

@end

