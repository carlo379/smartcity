//
//  Constants.m
//  Phonebook
//
//  Created by Carlos Martinez on 2/19/16.
//  Copyright © 2016 Carlos Martinez. All rights reserved.

#import "Constants.h"

// ENTITIES
NSString *const kUserEntityName = @"User";
// ATTRIBUTES
NSString *const kUserFirstNameAttributeKey = @"firstName";
NSString *const kUserLastNameAttributeKey = @"lastName";
NSString *const kUserUserNameAttributeKey = @"userName";
NSString *const kUserLocationsAttributeKey = @"locations";

//Administrators
NSString *const kAdministratorUID = @"a146ed15-6dc6-434e-8cda-a02e5ad37b20";

// Session Capture
char *const kCaptureSessionQueue = "captureSessionQueue";

// Animation
float const kDefaultAnimationTime = 0.2;
float const kDefaultDelayTime = 0.0;