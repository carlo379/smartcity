//
//  Constants.h
//  SmartCity
//
//  Created by Carlos Martinez on 2/25/16.
//  Copyright © 2016 Carlos Martinez. All rights reserved.
//
//  This header is included in 'PrefixHeader.pch'
//

@import UIKit;

// ENTITIES
extern NSString *const kUserEntityName;
// ATTRIBUTES
extern NSString *const kUserFirstNameAttributeKey;
extern NSString *const kUserLastNameAttributeKey;
extern NSString *const kUserUserNameAttributeKey;
extern NSString *const kUserLocationsAttributeKey;
// ADMINISTRATORS
extern NSString *const kAdministratorUID;
// Capture Session
extern char *const kCaptureSessionQueue;
// Animation
extern float const kDefaultAnimationTime;
extern float const kDefaultDelayTime;