//
//  MapViewAnnotation.m
//  Angels
//
//  Created by Carlos Martinez on 8/29/14.
//  Copyright (c) 2014 Codengineers. All rights reserved.
//

#import "MapViewAnnotation.h"
#import "Spot.h"
#import "Location.h"

@interface MapViewAnnotation()

@end

@implementation MapViewAnnotation

-(id) initWithRecord:(Spot *)spot
{
    self = [super init];
    // Set Property spot with passed spot
    self.spot = spot;
    
    // Required Properties by MKAnnotation Protocol
    _title = spot.place;
    
    Location  *locationObject = [spot valueForKeyPath:@"location"];
    
    CLLocation *questionLocation = (CLLocation *)locationObject.location;
    CLLocationCoordinate2D qLocation = CLLocationCoordinate2DMake(questionLocation.coordinate.latitude, questionLocation.coordinate.longitude);
    
    _coordinate = qLocation;
    
    _subtitle = [spot.rating stringValue];
    
    NSSet  *photoAll = [spot valueForKeyPath:@"medias"];
    NSArray *photoAllArray = [photoAll allObjects];
    
    if(photoAllArray.count > 0) {
        _image = [photoAllArray lastObject];
    } else {
        _image = nil;
    }
    
    return self;
}

@end
