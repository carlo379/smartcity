//
//  MapViewAnnotation.h
//  Angels
//
//  Created by Carlos Martinez on 8/29/14.
//  Copyright (c) 2014 Codengineers. All rights reserved.
//
@import MapKit;
@class Spot;

@interface MapViewAnnotation : NSObject<MKAnnotation>

@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;  // Required Property by MKAnnotation Protocol
@property (nonatomic, copy) NSString *subtitle;
@property (nonatomic, copy) NSString *phoneNumber;
@property (nonatomic, retain) NSURL *url;

@property (nonatomic, copy) UIImage *image;
@property (nonatomic, strong) MKMapItem *item;
@property (nonatomic, strong) Spot *spot;

-(id) initWithRecord:(Spot *)spot;

@end