//
//  CommentsTableViewCell.h
//  SmartCity
//
//  Created by Carlos Martinez on 3/28/16.
//  Copyright © 2016 Carlos Martinez. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HCSStarRatingView;

@interface CommentsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *commentLabel;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *ratingView;

@end
