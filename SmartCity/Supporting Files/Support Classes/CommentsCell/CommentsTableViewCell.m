//
//  CommentsTableViewCell.m
//  SmartCity
//
//  Created by Carlos Martinez on 3/28/16.
//  Copyright © 2016 Carlos Martinez. All rights reserved.
//

#import "CommentsTableViewCell.h"

@implementation CommentsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
