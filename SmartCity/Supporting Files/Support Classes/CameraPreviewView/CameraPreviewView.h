//
//  CameraPreviewView.h
//  SmartCity
//
//  Created by Carlos Martinez on 3/13/16.
//  Copyright © 2016 Carlos Martinez. All rights reserved.
//

@import UIKit;
@class AVCaptureSession;

@interface CameraPreviewView : UIView

@property (nonatomic) AVCaptureSession *session;

@end
