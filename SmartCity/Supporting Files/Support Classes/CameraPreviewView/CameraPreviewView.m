//
//  CameraPreviewView.m
//  SmartCity
//
//  Created by Carlos Martinez on 3/13/16.
//  Copyright © 2016 Carlos Martinez. All rights reserved.
//

#import "CameraPreviewView.h"
@import AVFoundation;

@implementation CameraPreviewView

+ (Class)layerClass
{
    return [AVCaptureVideoPreviewLayer class];
}

- (AVCaptureSession *)session
{
    AVCaptureVideoPreviewLayer *previewLayer = (AVCaptureVideoPreviewLayer *)self.layer;
    return previewLayer.session;
}

- (void)setSession:(AVCaptureSession *)session
{
    AVCaptureVideoPreviewLayer *previewLayer = (AVCaptureVideoPreviewLayer *)self.layer;
    previewLayer.session = session;
}


@end
