//
//  User+CoreDataProperties.m
//  SmartCity
//
//  Created by Carlos Martinez on 2/26/16.
//  Copyright © 2016 Carlos Martinez. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "User+CoreDataProperties.h"

@implementation User (CoreDataProperties)

@dynamic firstName;
@dynamic lastName;
@dynamic userName;
@dynamic locations;

@end
