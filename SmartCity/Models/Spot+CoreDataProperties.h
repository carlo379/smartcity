//
//  Spot+CoreDataProperties.h
//  SmartCity
//
//  Created by Carlos Martinez on 2/26/16.
//  Copyright © 2016 Carlos Martinez. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Spot.h"

NS_ASSUME_NONNULL_BEGIN

@interface Spot (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *comment;
@property (nullable, nonatomic, retain) NSString *place;
@property (nullable, nonatomic, retain) NSString *placeDescription;
@property (nullable, nonatomic, retain) NSNumber *rating;
@property (nullable, nonatomic, retain) NSSet<Media *> *medias;
@property (nullable, nonatomic, retain) Location *locationForSpot;

@end

@interface Spot (CoreDataGeneratedAccessors)

- (void)addMediasObject:(Media *)value;
- (void)removeMediasObject:(Media *)value;
- (void)addMedias:(NSSet<Media *> *)values;
- (void)removeMedias:(NSSet<Media *> *)values;

@end

NS_ASSUME_NONNULL_END
