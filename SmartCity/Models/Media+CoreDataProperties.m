//
//  Media+CoreDataProperties.m
//  SmartCity
//
//  Created by Carlos Martinez on 2/26/16.
//  Copyright © 2016 Carlos Martinez. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Media+CoreDataProperties.h"

@implementation Media (CoreDataProperties)

@dynamic data;
@dynamic mediaType;
@dynamic url;
@dynamic spotForMedia;

@end
