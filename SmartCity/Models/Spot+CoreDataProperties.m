//
//  Spot+CoreDataProperties.m
//  SmartCity
//
//  Created by Carlos Martinez on 2/26/16.
//  Copyright © 2016 Carlos Martinez. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Spot+CoreDataProperties.h"

@implementation Spot (CoreDataProperties)

@dynamic comment;
@dynamic place;
@dynamic placeDescription;
@dynamic rating;
@dynamic medias;
@dynamic locationForSpot;

@end
