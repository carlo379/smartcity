//
//  Spot.h
//  SmartCity
//
//  Created by Carlos Martinez on 2/25/16.
//  Copyright © 2016 Carlos Martinez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Location, Media;

NS_ASSUME_NONNULL_BEGIN

@interface Spot : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Spot+CoreDataProperties.h"
