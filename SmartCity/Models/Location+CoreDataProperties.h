//
//  Location+CoreDataProperties.h
//  SmartCity
//
//  Created by Carlos Martinez on 2/26/16.
//  Copyright © 2016 Carlos Martinez. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Location.h"

NS_ASSUME_NONNULL_BEGIN

@interface Location (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *event;
@property (nullable, nonatomic, retain) id location;
@property (nullable, nonatomic, retain) NSDate *time;
@property (nullable, nonatomic, retain) User *userForLocation;
@property (nullable, nonatomic, retain) Spot *spot;

@end

NS_ASSUME_NONNULL_END
