//
//  Media+CoreDataProperties.h
//  SmartCity
//
//  Created by Carlos Martinez on 2/26/16.
//  Copyright © 2016 Carlos Martinez. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Media.h"

NS_ASSUME_NONNULL_BEGIN

@interface Media (CoreDataProperties)

@property (nullable, nonatomic, retain) NSData *data;
@property (nullable, nonatomic, retain) NSString *mediaType;
@property (nullable, nonatomic, retain) NSString *url;
@property (nullable, nonatomic, retain) Spot *spotForMedia;

@end

NS_ASSUME_NONNULL_END
